<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\TripType;
use App\Http\Controllers\Controller;

class TripTypeController extends Controller
{
    public function index()
    {
        $data = TripType::paginate(20);
        return view('admin.TripType.listTripType', compact('data'));
    }

    public function add()
    {
        

        return view('admin.TripType.addTripType', compact());
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['trip_type'] = $data['trip_type'];
$saveData['slug'] = $data['slug'];
$saveData['status'] = $data['status'];


        $TripType = TripType::create($saveData);

        // return response()->json(['success' => true, 'data' => $TripType], 200);
        return redirect('/admin/trip_type')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = TripType::where('id', $id)->first();return view('admin.TripType.addTripType', compact('row', ));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['trip_type'] = $data['trip_type'];
$saveData['slug'] = $data['slug'];
$saveData['status'] = $data['status'];

        $row = TripType::where('id', $id)->first();
        if ($row){
            $TripType = TripType::where('id', $id)->update($saveData);
        }
        return redirect('/admin/trip_type')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = TripType::where('id', $request->id)->delete();
        return redirect('/admin/trip_type');

    }


    public function getData(){
        $data = TripType::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
