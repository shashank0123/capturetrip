<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogTable extends Migration

{
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->id();
$table->string('title')->nullable();
$table->string('text')->nullable();
$table->string('featured_image')->nullable();
$table->string('status')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
