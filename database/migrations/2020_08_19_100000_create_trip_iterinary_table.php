<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTripIterinaryTable extends Migration

{
    public function up()
    {
        Schema::create('trip_iterinarys', function (Blueprint $table) {
            $table->id();
$table->string('trip_id')->nullable();
$table->string('day')->nullable();
$table->string('details')->nullable();
$table->string('description')->nullable();
$table->string('status')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('trip_iterinarys');
    }
}
