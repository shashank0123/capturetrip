<?php 

use Illuminate\Support\Facades\Route;

Route::get('/trip_enquiry','Admin\TripEnquiryController@index');
    Route::get('trip_enquiry/add','Admin\TripEnquiryController@add');
    Route::post('trip_enquiry/add','Admin\TripEnquiryController@store');
    Route::get('trip_enquiry/edit/{id}','Admin\TripEnquiryController@edit');
    Route::post('trip_enquiry/edit/{id}','Admin\TripEnquiryController@update');
    Route::get('trip_enquiry/delete/{id}','Admin\TripEnquiryController@delete');
?>