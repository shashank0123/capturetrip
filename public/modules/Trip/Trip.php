<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
protected $fillable = ["trip_name","description","category_id","cover_image","cost","cost_excludes","cost_includes","other_details","start_price"," trip_duration"];
}