<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\TripGallery;
use App\Models\Trip;
use App\Http\Controllers\Controller;

class TripGalleryController extends Controller
{
    public function index()
    {
        $data = TripGallery::paginate(20);
        return view('admin.TripGallery.listTripGallery', compact('data'));
    }

    public function add()
    {

        $trips = Trip::all();

        return view('admin.TripGallery.addTripGallery', compact('trips'));
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
        $saveData['trip_id'] = $data['trip_id'];

        if (request()->hasFile('image')) {
            $path = request()->file('image')->store(
                'file',
                'public'
            );

            $data['image'] = \Storage::disk('public')->url($path);
        }
        $saveData['image'] = $data['image'];
        $saveData['status'] = $data['status'];


        $TripGallery = TripGallery::create($saveData);

        // return response()->json(['success' => true, 'data' => $TripGallery], 200);
        return redirect('/admin/trip_gallery')->with('successMsg', 'Data has been saved.');
    }

    public function edit($id)
    {
        $row = TripGallery::where('id', $id)->first();
        $trips = Trip::all();
        return view('admin.TripGallery.addTripGallery', compact('row', 'trips'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
        $saveData['trip_id'] = $data['trip_id'];
        $saveData['image'] = $data['image'];
        $saveData['status'] = $data['status'];

        $row = TripGallery::where('id', $id)->first();
        if ($row) {
            $TripGallery = TripGallery::where('id', $id)->update($saveData);
        }
        return redirect('/admin/trip_gallery')->with('successMsg', 'Data has been updated.');
    }

    public function delete(Request $request)
    {
        $delete = TripGallery::where('id', $request->id)->delete();
        return redirect('/admin/trip_gallery');
    }


    public function getData()
    {
        $data = TripGallery::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
