<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\TripCategory;
use App\Http\Controllers\Controller;

class TripCategoryController extends Controller
{
    public function index()
    {
        $data = TripCategory::paginate(20);
        return view('admin.TripCategory.listTripCategory', compact('data'));
    }

    public function add()
    {
        

        return view('admin.TripCategory.addTripCategory', compact());
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['category_name'] = $data['category_name'];
$saveData['slug'] = $data['slug'];
$saveData['status'] = $data['status'];


        $TripCategory = TripCategory::create($saveData);

        // return response()->json(['success' => true, 'data' => $TripCategory], 200);
        return redirect('/admin/trip_category')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = TripCategory::where('id', $id)->first();return view('admin.TripCategory.addTripCategory', compact('row', ));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['category_name'] = $data['category_name'];
$saveData['slug'] = $data['slug'];
$saveData['status'] = $data['status'];

        $row = TripCategory::where('id', $id)->first();
        if ($row){
            $TripCategory = TripCategory::where('id', $id)->update($saveData);
        }
        return redirect('/admin/trip_category')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = TripCategory::where('id', $request->id)->delete();
        return redirect('/admin/trip_category');

    }


    public function getData(){
        $data = TripCategory::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
