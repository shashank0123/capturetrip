<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTripTypeTable extends Migration

{
    public function up()
    {
        Schema::create('trip_types', function (Blueprint $table) {
            $table->id();
$table->string('trip_type')->nullable();
$table->string('slug')->nullable();
$table->string('status')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('trip_types');
    }
}
