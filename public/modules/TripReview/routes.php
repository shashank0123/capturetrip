<?php 

use Illuminate\Support\Facades\Route;

Route::get('/trip_review','Admin\TripReviewController@index');
    Route::get('trip_review/add','Admin\TripReviewController@add');
    Route::post('trip_review/add','Admin\TripReviewController@store');
    Route::get('trip_review/edit/{id}','Admin\TripReviewController@edit');
    Route::post('trip_review/edit/{id}','Admin\TripReviewController@update');
    Route::get('trip_review/delete/{id}','Admin\TripReviewController@delete');
?>