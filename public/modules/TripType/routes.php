<?php 

use Illuminate\Support\Facades\Route;

Route::get('/trip_type','Admin\TripTypeController@index');
    Route::get('trip_type/add','Admin\TripTypeController@add');
    Route::post('trip_type/add','Admin\TripTypeController@store');
    Route::get('trip_type/edit/{id}','Admin\TripTypeController@edit');
    Route::post('trip_type/edit/{id}','Admin\TripTypeController@update');
    Route::get('trip_type/delete/{id}','Admin\TripTypeController@delete');
?>