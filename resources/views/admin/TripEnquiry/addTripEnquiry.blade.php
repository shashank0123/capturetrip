@extends('admin.layouts.admin')
@section('content')
<div class='col-md-12' style=''>
<Form action='/admin/trip_enquiry/add' method='post' enctype='multipart/form-data'>
@csrf
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Name</label>
                <input class='form-control btn-square' type='text' name='name' placeholder='Enter Name'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Email</label>
                <input class='form-control btn-square' type='text' name='email' placeholder='Enter Email'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Phone</label>
                <input class='form-control btn-square' type='text' name='phone' placeholder='Enter Phone'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Destination</label>
                <input class='form-control btn-square' type='text' name='destination' placeholder='Enter Destination'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
</div>
    <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <button class='btn btn-primary' type='submit' data-original-title='Save And Return' title=''>Save</button>
            <a href='/admin/trip_enquiry' class='btn btn-secondary' data-original-title='Cancel And Go Back' title=''>Cancel</a>
            <p style='display:none' class='help-block'>Press Submit to save</p>
        </div>
    </div>
    </Form>
@endsection