<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\TripIterinary;
use App\Models\Trip;
use App\Http\Controllers\Controller;

class TripIterinaryController extends Controller
{
    public function index()
    {
        $data = TripIterinary::paginate(20);
        return view('admin.TripIterinary.listTripIterinary', compact('data'));
    }

    public function add()
    {
        
$trips = Trip::all();

        return view('admin.TripIterinary.addTripIterinary', compact('trips'));
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['trip_id'] = $data['trip_id'];
$saveData['day'] = $data['day'];
$saveData['details'] = $data['details'];
$saveData['description'] = $data['description'];
$saveData['status'] = $data['status'];


        $TripIterinary = TripIterinary::create($saveData);

        // return response()->json(['success' => true, 'data' => $TripIterinary], 200);
        return redirect('/admin/trip_iterinary')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = TripIterinary::where('id', $id)->first();$trips = Trip::all();
return view('admin.TripIterinary.addTripIterinary', compact('row', 'trips'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['trip_id'] = $data['trip_id'];
$saveData['day'] = $data['day'];
$saveData['details'] = $data['details'];
$saveData['description'] = $data['description'];
$saveData['status'] = $data['status'];

        $row = TripIterinary::where('id', $id)->first();
        if ($row){
            $TripIterinary = TripIterinary::where('id', $id)->update($saveData);
        }
        return redirect('/admin/trip_iterinary')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = TripIterinary::where('id', $request->id)->delete();
        return redirect('/admin/trip_iterinary');

    }


    public function getData(){
        $data = TripIterinary::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
