
<html lang="en-US" class="Chrome Chrome84 cssanimations csstransitions no-touchevents wf-opensans-n7-active wf-opensans-n4-active wf-opensans-n5-active wf-opensans-n6-active wf-opensans-i4-active wf-opensans-i5-active wf-opensans-n1-active wf-opensans-n2-active wf-opensans-n3-active wf-opensans-i7-active js_active vc_desktop vc_transform vc_transform wf-opensans-i1-active wf-opensans-i2-active wf-opensans-i3-active wf-opensans-n8-active wf-opensans-n9-active wf-opensans-i8-active wf-opensans-i9-active wf-opensans-i6-active wf-active">

<head>
  <meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="format-detection" content="telephone=no" />
<meta https-equiv="x-dns-prefetch-control" content="on" />
<link rel="dns-prefetch" href="//fonts.googleapis.com" />
<link rel="dns-prefetch" href="//fonts.gstatic.com" />
<link rel="dns-prefetch" href="//0.gravatar.com/" />
<link rel="dns-prefetch" href="//2.gravatar.com/" />
<link rel="dns-prefetch" href="//www.youtube.com/" />
<link rel="dns-prefetch" href="//1.gravatar.com/" />
<title>
    Capture A trip - India's Coolest Travel Community for Group Trips,Treks
    &amp; Biking Expeditions
</title>
  <!-- This site is optimized with the Yoast SEO plugin v13.1 - https://yoast.com/wordpress/plugins/seo/ -->
  <meta name="description" content="Book customized trips, group trips &amp; treks to Himachal Pradesh, Uttarakhand, Ladakh &amp; across India. Explore culture, things to do &amp; offbeat places to visit" />
  <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
  <link rel="canonical" href="" />
  <meta property="og:locale" content="en_US" />
  <meta property="og:type" content="website" />
  <meta property="og:title" content="WanderOn Travel Community - India's Coolest Travel Community" />
  <meta property="og:description" content="Book customized trips, group trips &amp; treks to Himachal Pradesh, Uttarakhand, Ladakh &amp; across India. Explore culture, things to do &amp; offbeat places to visit" />
  <meta property="og:url" content="" />
  <meta property="og:site_name" content="WanderOn" />
  <meta name="twitter:card" content="summary_large_image" />
  <meta name="twitter:description" content="Book customized trips, group trips &amp; treks to Himachal Pradesh, Uttarakhand, Ladakh &amp; across India. Explore culture, things to do &amp; offbeat places to visit" />
  <meta name="twitter:title" content="WanderOn - India's Coolest Travel Community for Group Trips,Treks &amp; Biking Expeditions" />
  <meta name="google-site-verification" content="" />

  <link rel="shortcut icon" href="/assets/images/favicon.png" />

  <link rel="stylesheet" id="fvm-header-0-css" href="/assets/css/styles.css" type="text/css" media="all" />
  <link rel="stylesheet" id="fvm-header-0-css" href="/assets/css/header.min.css" type="text/css" media="all" />
  <script type="text/javascript" src="assets/js/webfontloader.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/css/bootstrap-slider.min.css" integrity="sha512-3q8fi8M0VS+X/3n64Ndpp6Bit7oXSiyCnzmlx6IDBLGlY5euFySyJ46RUlqIVs0DPCGOypqP8IRk/EyPvU28mQ==" crossorigin="anonymous" />
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:100italic,200italic,300italic,400italic,500italic,600italic,700italic,800italic,900italic,100,200,300,400,500,600,700,800,900" media="all" />
  <script type="text/javascript">
    WebFontConfig = {
      timeout: 2000,
    };

    if (mk_typekit_id.length > 0) {
      WebFontConfig.typekit = {
        id: mk_typekit_id,
      };
    }

    if (mk_google_fonts.length > 0) {
      WebFontConfig.google = {
        families: mk_google_fonts,
      };
    }

    if (
      (mk_google_fonts.length > 0 || mk_typekit_id.length > 0) &&
      navigator.userAgent.indexOf("Speed Insights") == -1
    ) {
      WebFont.load(WebFontConfig);
    }
  </script>
  <script type="text/javascript" src="wp-content/uploads/cache/fvm/1592375791/out/header-f0058641a050d28b94dd899c6780665c0c05dbf9.min.js"></script>
  <script type="text/javascript" src="https://unpkg.com/axios@0.19.0/dist/axios.min.js"></script>
  <link rel="https://api.w.org/" href="wp-json/" />
  <link rel="alternate" type="application/json+oembed" href="wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.wanderon.in%2F" />
  <link rel="alternate" type="text/xml+oembed" href="wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.wanderon.in%2F&amp;format=xml" />
  <script>
    var isTest = false;
  </script>
  <meta itemprop="author" content="admin" />
  <meta itemprop="datePublished" content="April 4, 2016" />
  <meta itemprop="dateModified" content="May 27, 2020" />
  <meta itemprop="publisher" content="WanderOn" />
  <meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress." />
  <!--[if lte IE 9
      ]><link
        rel="stylesheet"
        type="text/css"
        href="wp-content/plugins/js_composer_theme/assets/css/vc_lte_ie9.min.css"
        media="screen"
    /><![endif]-->
  <link rel="icon" href="/assets/images/cropped-favicon-32x32.png" sizes="32x32" />
  <link rel="icon" href="/assets/images/cropped-favicon-192x192.png" sizes="192x192" />
  <link rel="apple-touch-icon-precomposed" href="/assets/images/cropped-favicon-180x180.png" />
  <meta name="msapplication-TileImage" content="/assets/images/cropped-favicon-270x270.png" />
  <meta name="generator" content="Jupiter 6.1.6" />

  <meta name="theme-color" content="#479f99" />


  <script async="" src="https://geocode.usefomo.com/json/?callback=fomo.geocodeResponse&amp;retry=1"></script>
  <script async="" src="https://e.fomo.com/api/v1/js-obj/ou4oHdosTUwDgXEYCLFMmA/events/geo_events.js?lng=en&amp;address=India"></script>
</head>

<body class="home page-template-default page page-id-82 wpb-js-composer js-comp-ver-5.5.5 vc_responsive" itemscope="itemscope" itemtype="https://schema.org/WebPage" data-adminbar="">
  <!-- Target for scroll anchors to achieve native browser bahaviour + possible enhancements like smooth scrolling -->
  <div id="top-of-page"></div>
  <div id="mk-boxed-layout">
    <div id="mk-theme-container">
      <header data-height="60" data-sticky-height="55" data-responsive-height="70" data-transparent-skin="light" data-header-style="1" data-sticky-style="fixed" data-sticky-offset="header" id="mk-header-1" class="mk-header header-style-1 header-align-left toolbar-true menu-hover-5 sticky-style-fixed mk-background-stretch full-header" role="banner" itemscope="itemscope" itemtype="https://schema.org/WPHeader">
    <div class="mk-header-holder">
        <div class="mk-header-toolbar">
            <div class="mk-header-toolbar-holder">
                <span class="header-toolbar-contact">
                    <svg class="mk-svg-icon" data-name="mk-moon-phone-3" data-cacheid="icon-5f39dd2968a3f" style="height: 16px; width: 16px;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                        <path d="M457.153 103.648c53.267 30.284 54.847 62.709 54.849 85.349v3.397c0 5.182-4.469 9.418-9.928 9.418h-120.146c-5.459 0-9.928-4.236-9.928-9.418v-11.453c0-28.605-27.355-33.175-42.449-35.605-15.096-2.426-52.617-4.777-73.48-4.777h-.14300000000000002c-20.862 0-58.387 2.35-73.48 4.777-15.093 2.427-42.449 6.998-42.449 35.605v11.453c0 5.182-4.469 9.418-9.926 9.418h-120.146c-5.457 0-9.926-4.236-9.926-9.418v-3.397c0-22.64 1.58-55.065 54.847-85.349 63.35-36.01 153.929-39.648 201.08-39.648l.077.078.066-.078c47.152 0 137.732 3.634 201.082 39.648zm-201.152 88.352c-28.374 0-87.443 2.126-117.456 38.519-30.022 36.383-105.09 217.481-38.147 217.481h311.201c66.945 0-8.125-181.098-38.137-217.481-30.018-36.393-89.1-38.519-117.461-38.519zm-.001 192c-35.346 0-64-28.653-64-64s28.654-64 64-64c35.347 0 64 28.653 64 64s-28.653 64-64 64z"></path>
                    </svg>
                    <a href="tel:+91-8814871652">+91-9560755823</a> </span><span class="header-toolbar-contact">
                    <svg class="mk-svg-icon" data-name="mk-moon-envelop" data-cacheid="icon-5f39dd2968c85" style="height: 16px; width: 16px;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                        <path d="M480 64h-448c-17.6 0-32 14.4-32 32v320c0 17.6 14.4 32 32 32h448c17.6 0 32-14.4 32-32v-320c0-17.6-14.4-32-32-32zm-32 64v23l-192 113.143-192-113.143v-23h384zm-384 256v-177.286l192 113.143 192-113.143v177.286h-384z"></path>
                    </svg>
                    <a href="mailto:hello@wanderon.in">hello@captureatrip.com</a>
                </span>
            </div>
        </div>
        <div class="mk-header-inner add-header-height">
            <div class="mk-header-bg"></div>
            <div class="mk-toolbar-resposnive-icon">
                <svg class="mk-svg-icon" data-name="mk-icon-chevron-down" data-cacheid="icon-5f39dd2968e68" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792">
                    <path d="M1683 808l-742 741q-19 19-45 19t-45-19l-742-741q-19-19-19-45.5t19-45.5l166-165q19-19 45-19t45 19l531 531 531-531q19-19 45-19t45 19l166 165q19 19 19 45.5t-19 45.5z"></path>
                </svg>
            </div>
            <div class="mk-header-nav-container one-row-style menu-hover-style-5" role="navigation" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement">
                <nav class="mk-main-navigation js-main-nav">
                    <ul id="menu-main-menu" class="main-navigation-ul dropdownJavascript">
                        <li id="menu-item-43100" class="menu-item menu-item-type-custom menu-item-object-custom has-mega-menu">
                            <a class="menu-item-link js-smooth-scroll" href="https://www.workcations.in/">WORKCATIONS</a>
                        </li>
                        <li id="menu-item-42974" class="menu-item menu-item-type-post_type menu-item-object-page has-mega-menu">
                            <a class="menu-item-link js-smooth-scroll" href="sanitised-taxi-services/">Book Sanitised Taxi</a>
                        </li>
                        <li id="menu-item-16417" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children no-mega-menu">
                            <a class="menu-item-link js-smooth-scroll" href="backpacking-trips/" aria-haspopup="true">Backpacking Trips</a>
                            <ul style="" class="sub-menu">
                                <li id="menu-item-38310" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children with-menu">
                                    <a class="menu-item-link js-smooth-scroll" href="leh-ladakh-trips/" aria-haspopup="true">Leh Ladakh</a><i class="menu-sub-level-arrow"><svg class="mk-svg-icon" data-name="mk-icon-angle-right" data-cacheid="icon-5f39dd296bf73" style="height: 16px; width: 5.7142857142857px;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 1792">
                                            <path d="M595 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"></path>
                                        </svg></i>
                                    <ul style="" class="sub-menu">
                                        <li id="menu-item-38311" class="menu-item menu-item-type-custom menu-item-object-custom">
                                            <a class="menu-item-link js-smooth-scroll" href="srinagar-leh-srinagar/">Srinagar Leh Srinagar</a>
                                        </li>
                                        <li id="menu-item-38312" class="menu-item menu-item-type-custom menu-item-object-custom">
                                            <a class="menu-item-link js-smooth-scroll" href="manali-leh-srinagar/">Manali Leh Srinagar</a>
                                        </li>
                                        <li id="menu-item-38313" class="menu-item menu-item-type-custom menu-item-object-custom">
                                            <a class="menu-item-link js-smooth-scroll" href="srinagar-leh-manali/">Srinagar Leh Manali</a>
                                        </li>
                                        <li id="menu-item-38314" class="menu-item menu-item-type-custom menu-item-object-custom">
                                            <a class="menu-item-link js-smooth-scroll" href="ladakh/">Manali Leh Manali</a>
                                        </li>
                                    </ul>
                                </li>
                                <li id="menu-item-38315" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children with-menu">
                                    <a class="menu-item-link js-smooth-scroll" href="spititrip/" aria-haspopup="true">Spiti Valley</a><i class="menu-sub-level-arrow"><svg class="mk-svg-icon" data-name="mk-icon-angle-right" data-cacheid="icon-5f39dd296c4ac" style="height: 16px; width: 5.7142857142857px;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 1792">
                                            <path d="M595 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"></path>
                                        </svg></i>
                                    <ul style="" class="sub-menu">
                                        <li id="menu-item-38316" class="menu-item menu-item-type-custom menu-item-object-custom">
                                            <a class="menu-item-link js-smooth-scroll" href="spitiwinter/">Spiti Valley Winter Expedition</a>
                                        </li>
                                        <li id="menu-item-38317" class="menu-item menu-item-type-custom menu-item-object-custom">
                                            <a class="menu-item-link js-smooth-scroll" href="spitiwinterbiking/">Spiti Winter Biking</a>
                                        </li>
                                        <li id="menu-item-38318" class="menu-item menu-item-type-custom menu-item-object-custom">
                                            <a class="menu-item-link js-smooth-scroll" href="spitivalley/">Spiti Valley RoadTrip</a>
                                        </li>
                                        <li id="menu-item-38319" class="menu-item menu-item-type-custom menu-item-object-custom">
                                            <a class="menu-item-link js-smooth-scroll" href="spitibiking/">Spiti Valley Biking Expedition</a>
                                        </li>
                                    </ul>
                                </li>
                                <li id="menu-item-38414" class="menu-item menu-item-type-custom menu-item-object-custom">
                                    <a class="menu-item-link js-smooth-scroll" href="bali/">Bali</a>
                                </li>
                                <li id="menu-item-16418" class="menu-item menu-item-type-custom menu-item-object-custom">
                                    <a class="menu-item-link js-smooth-scroll" href="bhutan">Bhutan</a>
                                </li>
                                <li id="menu-item-38159" class="menu-item menu-item-type-post_type menu-item-object-page">
                                    <a class="menu-item-link js-smooth-scroll" href="tawangroadtrip/">Tawang Road Trip</a>
                                </li>
                                <li id="menu-item-3676" class="menu-item menu-item-type-custom menu-item-object-custom">
                                    <a class="menu-item-link js-smooth-scroll" href="meghalayaroadtrip">Meghalaya</a>
                                </li>
                                <li id="menu-item-32096" class="menu-item menu-item-type-post_type menu-item-object-page">
                                    <a class="menu-item-link js-smooth-scroll" href="sikkim-roadtrip/">Sikkim Road Trip</a>
                                </li>
                            </ul>
                        </li>
                        <li id="menu-item-31718" class="menu-item menu-item-type-custom menu-item-object-custom has-mega-menu">
                            <a class="menu-item-link js-smooth-scroll" href="weekendgetaways">Weekend Delhi</a>
                        </li>
                        <li id="menu-item-31719" class="menu-item menu-item-type-custom menu-item-object-custom has-mega-menu">
                            <a class="menu-item-link js-smooth-scroll" href="weekend-getaways-from-bangalore">Weekend Bangalore</a>
                        </li>
                        <li id="menu-item-5930" class="menu-item menu-item-type-custom menu-item-object-custom no-mega-menu">
                            <a class="menu-item-link js-smooth-scroll" href="blogs">Blogs</a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="mk-nav-responsive-link">
                <div class="mk-css-icon-menu">
                    <div class="mk-css-icon-menu-line-1"></div>
                    <div class="mk-css-icon-menu-line-2"></div>
                    <div class="mk-css-icon-menu-line-3"></div>
                </div>
            </div>
            <div class="header-logo fit-logo-img add-header-height logo-is-responsive logo-has-sticky">
                <a href="" title="WanderOn"><img class="mk-desktop-logo dark-logo" title="Travel Community" alt="Travel Community" src="/assets/images/logo.png" /><img class="mk-desktop-logo light-logo" title="Travel Community" alt="Travel Community" src="wp-content/uploads/2018/11/white.png" />
                    <img class="mk-resposnive-logo" title="Travel Community" alt="Travel Community" src="/assets/images/logo.png" />
                    <img class="mk-sticky-logo" title="Travel Community" alt="Travel Community" src="/assets/images/logo.png" />
                </a>
            </div>
            <div class="mk-header-right"></div>
        </div>
        <div class="mk-responsive-wrap">
            <nav class="menu-main-menu-container">
                <ul id="menu-main-menu-1" class="mk-responsive-nav">
                    <li id="responsive-menu-item-42974" class="menu-item menu-item-type-post_type menu-item-object-page">
                        <a class="menu-item-link js-smooth-scroll" href="sanitised-taxi-services/">Book Sanitised Taxi</a>
                    </li>
                    <li id="responsive-menu-item-16417" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children">
                        <a class="menu-item-link js-smooth-scroll" href="backpacking-trips/">Backpacking Trips</a><span class="mk-nav-arrow mk-nav-sub-closed"><svg class="mk-svg-icon" data-name="mk-moon-arrow-down" data-cacheid="icon-5f39dd296f221" style="height: 16px; width: 16px;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                <path d="M512 192l-96-96-160 160-160-160-96 96 256 255.999z"></path>
                            </svg></span>
                        <ul class="sub-menu">
                            <li id="responsive-menu-item-38310" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children">
                                <a class="menu-item-link js-smooth-scroll" href="leh-ladakh-trips/">Leh Ladakh</a><span class="mk-nav-arrow mk-nav-sub-closed"><svg class="mk-svg-icon" data-name="mk-moon-arrow-down" data-cacheid="icon-5f39dd296f60c" style="height: 16px; width: 16px;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                        <path d="M512 192l-96-96-160 160-160-160-96 96 256 255.999z"></path>
                                    </svg></span>
                                <ul class="sub-menu">
                                    <li id="responsive-menu-item-38311" class="menu-item menu-item-type-custom menu-item-object-custom">
                                        <a class="menu-item-link js-smooth-scroll" href="srinagar-leh-srinagar/">Srinagar Leh Srinagar</a>
                                    </li>
                                    <li id="responsive-menu-item-38312" class="menu-item menu-item-type-custom menu-item-object-custom">
                                        <a class="menu-item-link js-smooth-scroll" href="manali-leh-srinagar/">Manali Leh Srinagar</a>
                                    </li>
                                    <li id="responsive-menu-item-38313" class="menu-item menu-item-type-custom menu-item-object-custom">
                                        <a class="menu-item-link js-smooth-scroll" href="srinagar-leh-manali/">Srinagar Leh Manali</a>
                                    </li>
                                    <li id="responsive-menu-item-38314" class="menu-item menu-item-type-custom menu-item-object-custom">
                                        <a class="menu-item-link js-smooth-scroll" href="ladakh/">Manali Leh Manali</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="responsive-menu-item-38315" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children">
                                <a class="menu-item-link js-smooth-scroll" href="spititrip/">Spiti Valley</a><span class="mk-nav-arrow mk-nav-sub-closed"><svg class="mk-svg-icon" data-name="mk-moon-arrow-down" data-cacheid="icon-5f39dd296fbeb" style="height: 16px; width: 16px;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                        <path d="M512 192l-96-96-160 160-160-160-96 96 256 255.999z"></path>
                                    </svg></span>
                                <ul class="sub-menu">
                                    <li id="responsive-menu-item-38316" class="menu-item menu-item-type-custom menu-item-object-custom">
                                        <a class="menu-item-link js-smooth-scroll" href="spitiwinter/">Spiti Valley Winter Expedition</a>
                                    </li>
                                    <li id="responsive-menu-item-38317" class="menu-item menu-item-type-custom menu-item-object-custom">
                                        <a class="menu-item-link js-smooth-scroll" href="spitiwinterbiking/">Spiti Winter Biking</a>
                                    </li>
                                    <li id="responsive-menu-item-38318" class="menu-item menu-item-type-custom menu-item-object-custom">
                                        <a class="menu-item-link js-smooth-scroll" href="spitivalley/">Spiti Valley RoadTrip</a>
                                    </li>
                                    <li id="responsive-menu-item-38319" class="menu-item menu-item-type-custom menu-item-object-custom">
                                        <a class="menu-item-link js-smooth-scroll" href="spitibiking/">Spiti Valley Biking Expedition</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="responsive-menu-item-38414" class="menu-item menu-item-type-custom menu-item-object-custom">
                                <a class="menu-item-link js-smooth-scroll" href="bali/">Bali</a>
                            </li>
                            <li id="responsive-menu-item-16418" class="menu-item menu-item-type-custom menu-item-object-custom">
                                <a class="menu-item-link js-smooth-scroll" href="bhutan">Bhutan</a>
                            </li>
                            <li id="responsive-menu-item-38159" class="menu-item menu-item-type-post_type menu-item-object-page">
                                <a class="menu-item-link js-smooth-scroll" href="tawangroadtrip/">Tawang Road Trip</a>
                            </li>
                            <li id="responsive-menu-item-3676" class="menu-item menu-item-type-custom menu-item-object-custom">
                                <a class="menu-item-link js-smooth-scroll" href="meghalayaroadtrip">Meghalaya</a>
                            </li>
                            <li id="responsive-menu-item-32096" class="menu-item menu-item-type-post_type menu-item-object-page">
                                <a class="menu-item-link js-smooth-scroll" href="sikkim-roadtrip/">Sikkim Road Trip</a>
                            </li>
                        </ul>
                    </li>
                    <li id="responsive-menu-item-31718" class="menu-item menu-item-type-custom menu-item-object-custom">
                        <a class="menu-item-link js-smooth-scroll" href="weekendgetaways">Weekend Delhi</a>
                    </li>
                    <li id="responsive-menu-item-31719" class="menu-item menu-item-type-custom menu-item-object-custom">
                        <a class="menu-item-link js-smooth-scroll" href="weekend-getaways-from-bangalore">Weekend Bangalore</a>
                    </li>
                    <li id="responsive-menu-item-5930" class="menu-item menu-item-type-custom menu-item-object-custom">
                        <a class="menu-item-link js-smooth-scroll" href="blogs">Blogs</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="mk-header-padding-wrapper"></div>
</header>     @yield('contents')
      @include('layouts.footer')   <div class="resize-triggers">
        <div class="expand-trigger">
          <div style="width: 1584px; height: 3498px;"></div>
        </div>
        <div class="contract-trigger"></div>
      </div>
    </div>
  </div>
  <div class="bottom-corner-btns js-bottom-corner-btns"></div>



  <script>
    jQuery(document).ready(function($) {
      $(document).on("click", ".plus", function(e) {
        // replace '.quantity' with document (without single quote)
        $input = $(this).prev("input.qty");
        var val = parseInt($input.val());
        var step = $input.attr("step");
        step = "undefined" !== typeof step ? parseInt(step) : 1;
        $input.val(val + step).change();
      });
      $(document).on(
        "click",
        ".minus", // replace '.quantity' with document (without single quote)
        function(e) {
          $input = $(this).next("input.qty");
          var val = parseInt($input.val());
          var step = $input.attr("step");
          step = "undefined" !== typeof step ? parseInt(step) : 1;
          if (val > 0) {
            $input.val(val - step).change();
          }
        }
      );
    });
  </script>
  <style id="fvm-footer-0" media="all">

  </style>
  <script type="text/javascript" src="/assets/js/min/full-scripts.6.1.6.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/bootstrap-slider.min.js" integrity="sha512-f0VlzJbcEB6KiW8ZVtL+5HWPDyW1+nJEjguZ5IVnSQkvZbwBt2RfCBY0CBO1PsMAqxxrG4Di6TfsCPP3ZRwKpA==" crossorigin="anonymous"></script>

  <script type="text/javascript" src="/assets/js/footer.min.js"></script>
  <script type="text/javascript">
    /*******************   GALLERY         **************/

    function createGallery() {
      var imagesHTML = "";
      for (let i = 0; i < gallery_images.length; i++) {
        imagesHTML +=
          '<img src="' + gallery_images[i] + '" class="trip_gallery_image">';
      }
      document.querySelector(".trip_gallery").innerHTML = imagesHTML;
      addEvents();
    }

    function addEvents() {
      let images = [];
      images = document.querySelectorAll(".trip_gallery_image");
      for (let i = 0; i < gallery_images.length; i++) {
        images[i].addEventListener("click", function() {
          createLightBox(i);
        });
      }
    }

    function createLightBox(i) {
      var lightbox = document.createElement("div");
      lightbox.className = "trip_gallery_lightbox";
      lightbox.innerHTML =
        '<div class="trip_gallery_lightbox_container"><div class="trip_gallery_lightbox_nav" id="lightbox_close">&times;</div><div class="trip_gallery_lightbox_nav" id="previous"><</div><div class="trip_gallery_lightbox_nav" id="next">></div><div class="lightbox_thumbnails"></div><div class="trip_gallery_lightbox_image_container"></div></div>';
      document.body.appendChild(lightbox);
      let lightboxImagesHTML = "";
      for (let i = 0; i < gallery_images.length; i++) {
        lightboxImagesHTML +=
          '<img src="' +
          gallery_images[i] +
          '" class="trip_gallery_lightbox_image_container_image">';
      }
      document.querySelector(
        ".trip_gallery_lightbox_image_container"
      ).innerHTML = lightboxImagesHTML;
      let lightboxThumbsHTML = "";
      for (let i = 0; i < gallery_images.length; i++) {
        lightboxThumbsHTML +=
          '<img src="' +
          gallery_images[i] +
          '" class="lightbox_thumbnails_image">';
      }
      document.querySelector(
        ".lightbox_thumbnails"
      ).innerHTML = lightboxThumbsHTML;
      addEventsThumbnail();

      let currentSlideIndex = 3;
      var lightbox_image = [];
      lightbox_image = document.querySelectorAll(
        ".trip_gallery_lightbox_image_container_image"
      );
      var thumbnail_image = document.querySelectorAll(
        ".lightbox_thumbnails_image"
      );
      thumbnail_image[i].classList.add("lightbox_thumbnails_image_active");
      lightbox_image[i].classList.add(
        "trip_gallery_lightbox_image_container_image_opaque"
      );
      currentSlideIndex = i;

      function nextImage() {
        lightbox_image[currentSlideIndex].classList.remove(
          "trip_gallery_lightbox_image_container_image_opaque"
        );
        thumbnail_image[currentSlideIndex].classList.remove(
          "lightbox_thumbnails_image_active"
        );
        currentSlideIndex++;
        console.log(currentSlideIndex);
        if (currentSlideIndex < gallery_images.length) {
          lightbox_image[currentSlideIndex].classList.add(
            "trip_gallery_lightbox_image_container_image_opaque"
          );
          thumbnail_image[currentSlideIndex].classList.add(
            "lightbox_thumbnails_image_active"
          );
        } else {
          currentSlideIndex = 0;
          lightbox_image[currentSlideIndex].classList.add(
            "trip_gallery_lightbox_image_container_image_opaque"
          );
          thumbnail_image[currentSlideIndex].classList.add(
            "lightbox_thumbnails_image_active"
          );
        }
        clearInterval(autoSlideShow);
        autoSlideShow = setInterval(nextImage, 3000);
      }

      function previousImage() {
        lightbox_image[currentSlideIndex].classList.remove(
          "trip_gallery_lightbox_image_container_image_opaque"
        );
        thumbnail_image[currentSlideIndex].classList.remove(
          "lightbox_thumbnails_image_active"
        );

        currentSlideIndex++;
        console.log(currentSlideIndex);
        if (currentSlideIndex >= 0) {
          lightbox_image[currentSlideIndex].classList.add(
            "trip_gallery_lightbox_image_container_image_opaque"
          );
          thumbnail_image[currentSlideIndex].classList.add(
            "lightbox_thumbnails_image_active"
          );
        } else {
          currentSlideIndex = gallery_images.length - 1;
          lightbox_image[currentSlideIndex].classList.add(
            "trip_gallery_lightbox_image_container_image_opaque"
          );
          thumbnail_image[currentSlideIndex].classList.add(
            "lightbox_thumbnails_image_active"
          );
        }
        clearInterval(autoSlideShow);
        autoSlideShow = setInterval(nextImage, 3000);
      }

      function setCurrentSlide(i) {
        lightbox_image[currentSlideIndex].classList.remove(
          "trip_gallery_lightbox_image_container_image_opaque"
        );
        thumbnail_image[currentSlideIndex].classList.remove(
          "lightbox_thumbnails_image_active"
        );
        currentSlideIndex = i;
        lightbox_image[i].classList.add(
          "trip_gallery_lightbox_image_container_image_opaque"
        );
        thumbnail_image[i].classList.add("lightbox_thumbnails_image_active");
        clearInterval(autoSlideShow);
        autoSlideShow = setInterval(nextImage, 3000);
      }

      function addEventsThumbnail() {
        let images = [];
        images = document.querySelectorAll(".lightbox_thumbnails_image");
        for (let i = 0; i < gallery_images.length; i++) {
          images[i].addEventListener("click", function() {
            setCurrentSlide(i);
          });
        }
      }

      function closeLightbox() {
        lightbox.innerHTML = "";
        document.body.removeChild(lightbox);
        clearInterval(autoSlideShow);
      }

      document.getElementById("next").addEventListener("click", nextImage);

      document
        .getElementById("previous")
        .addEventListener("click", previousImage);

      document
        .getElementById("lightbox_close")
        .addEventListener("click", closeLightbox);

      let autoSlideShow = setInterval(nextImage, 3000);

      let modal = document.querySelector(".trip_gallery_lightbox");

      window.onclick = function(event) {
        if (event.target == modal) {
          closeLightbox();
        }
      };
    }

    /**********************   GALLERY ENDS    ****************/

    function createPopupTripPage() {
      var popupWindowCloseTripPage = document.createElement("img");
      popupWindowCloseTripPage.src =
        "/assets/images/cross-white.svg";
      popupWindowCloseTripPage.className += "MyPopupClose";
      var popupBackgroundTripPage = document.createElement("div");
      popupBackgroundTripPage.className += "MyPopupContainer";
      var popupWindowTripPage = document.createElement("span");
      popupWindowTripPage.className += "MyPopup";
      var placePopupHereTripPage = document.querySelector("#theme-page");
      popupWindowTripPage.innerHTML =
        '<span class="MyPopupHeading">Why Choose WanderOn?</span><iframe width="100%" height ="100%" src="https://www.youtube.com/embed/BagUzmZTJ58?rel=0&enablejsapi=1&amp;origin=https%3A%2F%2Fwww.wanderon.in" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
      popupBackgroundTripPage.appendChild(popupWindowTripPage);
      popupBackgroundTripPage.appendChild(popupWindowCloseTripPage);
      placePopupHereTripPage.appendChild(popupBackgroundTripPage);
      popupBackgroundTripPage.addEventListener("click", function() {
        placePopupHereTripPage.removeChild(popupBackgroundTripPage);
      });
      document
        .querySelector(".MyPopupClose")
        .addEventListener("click", function() {
          placePopupHereTripPage.removeChild(popupBackgroundTripPage);
        });
    }
    if (document.querySelector("#tripPagePopup")) {
      document.querySelector("#tripPagePopup").innerHTML =
        "<img src='wp-content/uploads/2019/popup/video.jpg' width='300px'>";
      document
        .querySelector("#tripPagePopup")
        .addEventListener("click", createPopupTripPage);
    }
    /*let reqRead = new XMLHttpRequest();

      reqRead.onreadystatechange = () => {
        if (reqRead.readyState == XMLHttpRequest.DONE) {
          return  JSON.parse(reqRead.responseText).id;
        }
      };

      reqRead.open("GET", "https://api.jsonbin.io/b/5dbe7f6872486e253fecffb4", true);
      reqRead.setRequestHeader("secret-key", "$2b$10$cCeqQRzIRAySNNKlAYuT0e21Chom8xiHyknNRsB6JMJAJZzMlyZHm");
      reqRead.send();
      let leadId_value;*/

    function removeBlogs() {
      let i;
      let blogs = document.querySelectorAll(".blog_list_item");
      console.log(blogs.length);
      if (screen.width <= 850) {
        for (i = 3; i < blogs.length; i++) blogs[i].style.display = "none";
      }
    }
    removeBlogs();

    var racbButton = document.createElement("div");
    racbButton.className = "racb_button";
    racbButton.innerHTML = "Request a Call Back";
    if (document.URL.split("/")[3] !== "bookings")
      document.querySelector("#theme-page").appendChild(racbButton);

    document.addEventListener(
      "DOMContentLoaded",
      function() {
        var socialbar = document.createElement("div");
        document.body.appendChild(socialbar);
        socialbar.className = "social_icon_bar";
        socialbar.innerHTML =
          '<a href = "tel:8814871652" target = "_blank" ><div class="social_icon_bar_item" id="social_click_call"><img src="wp-content/uploads/2019/social/call.svg" class="social_icon_bar_icon"></div></a><a href = "https://wa.me/918814871652?text=Hey!%20WanderOn%20I%20am%20interested%20in%20your%20trips" target = "_blank" ><div class="social_icon_bar_item" id="social_click_whatsapp"><img src="wp-content/uploads/2019/social/whatsapp.svg" class="social_icon_bar_icon"></div></a><a href = "https://www.instagram.com/wander.on" target = "_blank" ><div class="social_icon_bar_item" id="social_click_instagram"><img src="wp-content/uploads/2019/social/instagram.svg" class="social_icon_bar_icon"></div></a><a href = "https://www.facebook.com/wander.on" target = "_blank" ><div class="social_icon_bar_item" id="social_click_facebook"><img src="wp-content/uploads/2019/social/facebook.svg" class="social_icon_bar_icon"></div></a><a href = "https://www.youtube.com/channel/UCWFU6ktfGKWhL2e2ItHKLFQ" target = "_blank" ><div class="social_icon_bar_item" id="social_click_youtube"><img src="wp-content/uploads/2019/social/youtube.svg" class="social_icon_bar_icon"></div></a><a href = "https://www.linkedin.com/company/wanderontravelcommunity" target = "_blank" ><div class="social_icon_bar_item" id="social_click_linkedin"><img src="wp-content/uploads/2019/social/linkedin.svg" class="social_icon_bar_icon"></div></a>';
        document
          .getElementById("social_click_call")
          .addEventListener("click", function() {
            window.dataLayer.push({
              event: "call_button_pressed",
              buttonId: "social_click_call",
            });
          });
        document
          .getElementById("social_click_whatsapp")
          .addEventListener("click", function() {
            window.dataLayer.push({
              event: "whatsapp_button_pressed",
              buttonId: "social_click_whatsapp",
            });
          });
        document
          .getElementById("social_click_instagram")
          .addEventListener("click", function() {
            window.dataLayer.push({
              event: "instagram_button_pressed",
              buttonId: "social_click_instagram",
            });
          });
        document
          .getElementById("social_click_facebook")
          .addEventListener("click", function() {
            window.dataLayer.push({
              event: "facebook_button_pressed",
              buttonId: "social_click_facebook",
            });
          });
        document
          .getElementById("social_click_youtube")
          .addEventListener("click", function() {
            window.dataLayer.push({
              event: "youtube_button_pressed",
              buttonId: "social_click_youtube",
            });
          });
        document
          .getElementById("social_click_linkedin")
          .addEventListener("click", function() {
            window.dataLayer.push({
              event: "linkedin_button_pressed",
              buttonId: "social_click_linkedin",
            });
          });
      },
      false
    );
  </script>
  <script src="https://img1.wsimg.com/tcc/tcc_l.combined.1.0.6.min.js"></script>
  <noscript><img height="1" width="1" style="display: none;" src="https://www.facebook.com/tr?id=254978735193181&amp;ev=PageView&amp;noscript=1" /></noscript>
  <div class="social_icon_bar">
    <a href="tel:8814871652" target="_blank">
      <div class="social_icon_bar_item" id="social_click_call">
        <img src="/assets/images/social/call.svg" class="social_icon_bar_icon" />
      </div>
    </a><a href="https://wa.me/918814871652?text=Hey!%20WanderOn%20I%20am%20interested%20in%20your%20trips" target="_blank">
      <div class="social_icon_bar_item" id="social_click_whatsapp">
        <img src="/assets/images/social/whatsapp.svg" class="social_icon_bar_icon" />
      </div>
    </a><a href="https://www.instagram.com/wander.on" target="_blank">
      <div class="social_icon_bar_item" id="social_click_instagram">
        <img src="/assets/images/social/instagram.svg" class="social_icon_bar_icon" />
      </div>
    </a><a href="https://www.facebook.com/wander.on" target="_blank">
      <div class="social_icon_bar_item" id="social_click_facebook">
        <img src="/assets/images/social/facebook.svg" class="social_icon_bar_icon" />
      </div>
    </a><a href="https://www.youtube.com/channel/UCWFU6ktfGKWhL2e2ItHKLFQ" target="_blank">
      <div class="social_icon_bar_item" id="social_click_youtube">
        <img src="/assets/images/social/youtube.svg" class="social_icon_bar_icon" />
      </div>
    </a><a href="https://www.linkedin.com/company/wanderontravelcommunity" target="_blank">
      <div class="social_icon_bar_item" id="social_click_linkedin">
        <img src="/assets/images/social/linkedin.svg" class="social_icon_bar_icon" />
      </div>
    </a>
  </div>
  <script type="text/javascript" id="">
    function addEffectOnButton() {
      document.querySelector(".racb_button").classList.add("racb_button_add");
      setTimeout(function() {
        document
          .querySelector(".racb_button")
          .classList.remove("racb_button_add");
      }, 15e3);
    }
    addEffectOnButton();
  </script>
  <div style="
        position: absolute;
        z-index: -10000;
        top: 0px;
        left: 0px;
        right: 0px;
        height: 3497px;
      "></div>
</body>

</html>
