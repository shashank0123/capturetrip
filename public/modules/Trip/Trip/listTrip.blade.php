@extends('admin.layouts.admin')
@section('content')
<div class='col-sm-12'>
                <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <a href='/admin/trip/add' class='btn btn-secondary' data-original-title='Add New' title=''>Add New</a>
            <p class='help-block'>Press Submit to save</p>
        </div>
    </div>
                <div class='card'>
                  <div class='card-header'>
                    <h5>Trip </h5>

                  </div>
                  <div class='card-body'>
                    <div class='table-responsive'>
                      <table class='table table-styling' id='advance-1'>
                        <thead>
                          <tr><th>Trip Name</th><th>Description</th><th>Category Id</th><th>Cover Image</th><th>Cost</th><th>Cost Excludes</th><th>Cost Includes</th><th>Other Details</th><th>Start Price</th><th> Trip Duration</th><th>Action</th></tr>
                        </thead>
                        <tbody>
                        @foreach ($data as  $value) {
                        <tr><td>{{$value->trip_name}}</td><td>{{$value->description}}</td><td>{{$value->category_id}}</td><td>{{$value->cover_image}}</td><td>{{$value->cost}}</td><td>{{$value->cost_excludes}}</td><td>{{$value->cost_includes}}</td><td>{{$value->other_details}}</td><td>{{$value->start_price}}</td><td>{{$value-> trip_duration}}</td><td><a href='/admin/trip/edit/{{$value->id}}' class='btn btn-primary' data-original-title='' title=''>Edit</a><a href='/admin/trip/delete/{{$value->id}}' class='btn btn-danger' data-original-title='' title=''>delete</a></td></tr>@endforeach</tbody>


                      </table>
                    </div>
                  </div>
                </div>
              </div>
@endsection