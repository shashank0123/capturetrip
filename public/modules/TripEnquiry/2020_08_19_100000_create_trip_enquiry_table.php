<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTripEnquiryTable extends Migration

{
    public function up()
    {
        Schema::create('trip_enquirys', function (Blueprint $table) {
            $table->id();
$table->string('name')->nullable();
$table->string('email')->nullable();
$table->string('phone')->nullable();
$table->string('destination')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('trip_enquirys');
    }
}
