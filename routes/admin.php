<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('updatewebsite', 'WebsiteUpdateController@updateSite');


Route::get('/login', 'Admin\Auth\AdminLoginController@showLoginForm')->name('admin.login');
Route::post('/login', 'Admin\Auth\AdminLoginController@login')->name('admin.login.submit');
Route::get('/home', 'Admin\AdminController@index')->name('admin.home');


Route::group(['middleware' => ['auth:admin']], function () {
    Route::get('/dashboard', 'Admin\DashboardController@getDashboard');
    
    Route::get('/trip_type','Admin\TripTypeController@index');
    Route::get('trip_type/add','Admin\TripTypeController@add');
    Route::post('trip_type/add','Admin\TripTypeController@store');
    Route::get('trip_type/edit/{id}','Admin\TripTypeController@edit');
    Route::post('trip_type/edit/{id}','Admin\TripTypeController@update');
    Route::get('trip_type/delete/{id}','Admin\TripTypeController@delete');  
Route::get('/trip_category','Admin\TripCategoryController@index');
    Route::get('trip_category/add','Admin\TripCategoryController@add');
    Route::post('trip_category/add','Admin\TripCategoryController@store');
    Route::get('trip_category/edit/{id}','Admin\TripCategoryController@edit');
    Route::post('trip_category/edit/{id}','Admin\TripCategoryController@update');
    Route::get('trip_category/delete/{id}','Admin\TripCategoryController@delete');  
Route::get('/trip','Admin\TripController@index');
    Route::get('trip/add','Admin\TripController@add');
    Route::post('trip/add','Admin\TripController@store');
    Route::get('trip/edit/{id}','Admin\TripController@edit');
    Route::post('trip/edit/{id}','Admin\TripController@update');
    Route::get('trip/delete/{id}','Admin\TripController@delete');  
Route::get('/trip_iterinary','Admin\TripIterinaryController@index');
    Route::get('trip_iterinary/add','Admin\TripIterinaryController@add');
    Route::post('trip_iterinary/add','Admin\TripIterinaryController@store');
    Route::get('trip_iterinary/edit/{id}','Admin\TripIterinaryController@edit');
    Route::post('trip_iterinary/edit/{id}','Admin\TripIterinaryController@update');
    Route::get('trip_iterinary/delete/{id}','Admin\TripIterinaryController@delete');  
Route::get('/trip_review','Admin\TripReviewController@index');
    Route::get('trip_review/add','Admin\TripReviewController@add');
    Route::post('trip_review/add','Admin\TripReviewController@store');
    Route::get('trip_review/edit/{id}','Admin\TripReviewController@edit');
    Route::post('trip_review/edit/{id}','Admin\TripReviewController@update');
    Route::get('trip_review/delete/{id}','Admin\TripReviewController@delete');  
Route::get('/trip_gallery','Admin\TripGalleryController@index');
    Route::get('trip_gallery/add','Admin\TripGalleryController@add');
    Route::post('trip_gallery/add','Admin\TripGalleryController@store');
    Route::get('trip_gallery/edit/{id}','Admin\TripGalleryController@edit');
    Route::post('trip_gallery/edit/{id}','Admin\TripGalleryController@update');
    Route::get('trip_gallery/delete/{id}','Admin\TripGalleryController@delete');  
Route::get('/trip_enquiry','Admin\TripEnquiryController@index');
    Route::get('trip_enquiry/add','Admin\TripEnquiryController@add');
    Route::post('trip_enquiry/add','Admin\TripEnquiryController@store');
    Route::get('trip_enquiry/edit/{id}','Admin\TripEnquiryController@edit');
    Route::post('trip_enquiry/edit/{id}','Admin\TripEnquiryController@update');
    Route::get('trip_enquiry/delete/{id}','Admin\TripEnquiryController@delete');  
Route::get('/blog','Admin\BlogController@index');
    Route::get('blog/add','Admin\BlogController@add');
    Route::post('blog/add','Admin\BlogController@store');
    Route::get('blog/edit/{id}','Admin\BlogController@edit');
    Route::post('blog/edit/{id}','Admin\BlogController@update');
    Route::get('blog/delete/{id}','Admin\BlogController@delete');  
//{Route to be added here from automation}
});
