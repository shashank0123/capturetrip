<?php 

use Illuminate\Support\Facades\Route;

Route::get('/blog','Admin\BlogController@index');
    Route::get('blog/add','Admin\BlogController@add');
    Route::post('blog/add','Admin\BlogController@store');
    Route::get('blog/edit/{id}','Admin\BlogController@edit');
    Route::post('blog/edit/{id}','Admin\BlogController@update');
    Route::get('blog/delete/{id}','Admin\BlogController@delete');
?>