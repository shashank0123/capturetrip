<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTripReviewTable extends Migration

{
    public function up()
    {
        Schema::create('trip_reviews', function (Blueprint $table) {
            $table->id();
$table->string('trip_id')->nullable();
$table->string('name')->nullable();
$table->string('email')->nullable();
$table->string('phone')->nullable();
$table->string('title')->nullable();
$table->string('rating')->nullable();
$table->string('details')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('trip_reviews');
    }
}
