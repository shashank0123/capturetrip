@extends('admin.layouts.admin')
@section('content')
<div class='col-md-12' style=''>
<Form action='/admin/trip/add' method='post' enctype='multipart/form-data'>
@csrf
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Trip Name</label>
                <input class='form-control btn-square' type='text' name='trip_name' placeholder='Enter Trip Name'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Description</label>
                <input class='form-control btn-square' type='text' name='description' placeholder='Enter Description'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='col-md-12'>
        <div class='form-group ui-draggable-handle' style='position: static;'>
            <label for='formcontrol-select1'>TripCategory</label>
            <select class='form-control btn-square' name='category_id' id='category_id'>
                @foreach ($tripcategorys as $item)
                    <option value='{{$item->id}}'>{{$item->category_name}}</option>
                @endforeach

            </select>
            <p style='display: none' class='help-block'>Error Message</p>
        </div>
    </div><div class='form-group row'>
                    <label class='col-lg-12 control-label text-lg-left' for='cover_image'>File</label>
                    <div class='col-lg-12'>
                        <input id='cover_image' name='cover_image' class='input-file' type='file'>
                    </div>
                    </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Cost</label>
                <input class='form-control btn-square' type='text' name='cost' placeholder='Enter Cost'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Cost Excludes</label>
                <input class='form-control btn-square' type='text' name='cost_excludes' placeholder='Enter Cost Excludes'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Cost Includes</label>
                <input class='form-control btn-square' type='text' name='cost_includes' placeholder='Enter Cost Includes'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Other Details</label>
                <input class='form-control btn-square' type='text' name='other_details' placeholder='Enter Other Details'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Start Price</label>
                <input class='form-control btn-square' type='text' name='start_price' placeholder='Enter Start Price'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'> Trip Duration</label>
                <input class='form-control btn-square' type='text' name=' trip_duration' placeholder='Enter  Trip Duration'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
</div>
    <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <button class='btn btn-primary' type='submit' data-original-title='Save And Return' title=''>Save</button>
            <a href='/admin/trip' class='btn btn-secondary' data-original-title='Cancel And Go Back' title=''>Cancel</a>
            <p style='display:none' class='help-block'>Press Submit to save</p>
        </div>
    </div>
    </Form>
@endsection