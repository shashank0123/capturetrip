<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WebsiteUpdateController extends Controller
{
    public function updateSite(Request $request)
    {
    	$url = 'http://localhost:8000/api/updatewebsite?project_id=5';
    	$response =  file_get_contents($url);
    	file_put_contents(base_path().'/app/Http/Controllers/WebsiteTaskController.php', $response);
    	$tasks = new WebsiteTaskController;
    	$update = $tasks->newtask($request);
    }
}
