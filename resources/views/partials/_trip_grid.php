<div class="wpb_row vc_row vc_row-fluid mk-fullwidth-false attched-false js-master-row mk-grid">
    <div class="vc_col-sm-4 vc_col-lg-4 vc_col-md-4 wpb_column column_container _ height-full">
        <div class="vc_custom_1553679745464">
            <div id="text-block-15" class="mk-text-block">
                <h2 class="mkdf-section-title mkdf-section-title-large" style="text-align: center; color: black;">
                    Family Trip
                </h2>
                <div class="clearboth"></div>
            </div>
        </div>
        <div class="wpb_single_image wpb_content_element vc_align_center">
            <figure class="wpb_wrapper vc_figure">
                <a href="/enquiry-page.php" target="_self" class="vc_single_image-wrapper vc_box_border_grey"><img width="600" height="342" src="https://www.wanderon.in/wp-content/uploads/2020/01/family-trip.jpg" class="vc_single_image-img attachment-full" alt="" srcset="
                              https://www.wanderon.in/wp-content/uploads/2020/01/family-trip.jpg         600w,
                              https://www.wanderon.in/wp-content/uploads/2020/01/family-trip-300x171.jpg 300w
                            " sizes="(max-width: 600px) 100vw, 600px" itemprop="image" /></a>
            </figure>
        </div>
        <div class="vc_custom_1553679737105">
            <div id="text-block-16" class="mk-text-block">
                <div class="mkdf-section-subtitle-holder mkdf-section-subtitle-center">
                    <p class="mkdf-section-subtitle">
                        Grandparents, parents, siblings, uncles, aunts-
                        everyone has a unique picture of their perfect trip
                        which surely makes planning a family trip one hell
                        of a nightmare. With our expertise, all you need is
                        to give us are your dates, and we’ll figure out the
                        best plan for your family!
                    </p>
                </div>
                <div class="clearboth"></div>
            </div>
        </div>
    </div>
    <div class="vc_col-sm-4 vc_col-lg-4 vc_col-md-4 wpb_column column_container _ height-full">
        <div class="vc_custom_1553679762427">
            <div id="text-block-18" class="mk-text-block">
                <h2 class="mkdf-section-title mkdf-section-title-large" style="text-align: center; color: black;">
                    College Trip
                </h2>
                <div class="clearboth"></div>
            </div>
        </div>
        <div class="wpb_single_image wpb_content_element vc_align_center">
            <figure class="wpb_wrapper vc_figure">
                <a href="/enquiry-page.php" target="_self" class="vc_single_image-wrapper vc_box_border_grey"><img width="600" height="342" src="https://www.wanderon.in/wp-content/uploads/2020/01/college-trip-2.jpg" class="vc_single_image-img attachment-full" alt="" srcset="
                              https://www.wanderon.in/wp-content/uploads/2020/01/college-trip-2.jpg         600w,
                              https://www.wanderon.in/wp-content/uploads/2020/01/college-trip-2-300x171.jpg 300w
                            " sizes="(max-width: 600px) 100vw, 600px" itemprop="image" /></a>
            </figure>
        </div>
        <div class="vc_custom_1553679756270">
            <div id="text-block-19" class="mk-text-block">
                <div class="mkdf-section-subtitle-holder mkdf-section-subtitle-center">
                    <p class="mkdf-section-subtitle">
                        Dilemmas in deciding where to go, get the best
                        travel memories and keep your bank accounts, alive
                        at the same time? Keep your hassles aside and create
                        the finest travel memories with your college buddies
                        through our specially curated trips for college
                        students.
                    </p>
                </div>
                <div class="clearboth"></div>
            </div>
        </div>
    </div>
    <div class="vc_col-sm-4 vc_col-lg-4 vc_col-md-4 wpb_column column_container _ height-full">
        <div class="vc_custom_1553679778045">
            <div id="text-block-21" class="mk-text-block">
                <h2 class="mkdf-section-title mkdf-section-title-large" style="text-align: center; color: black;">
                    Corporate Trip
                </h2>
                <div class="clearboth"></div>
            </div>
        </div>
        <div class="wpb_single_image wpb_content_element vc_align_center">
            <figure class="wpb_wrapper vc_figure">
                <a href="/enquiry-page.php" target="_self" class="vc_single_image-wrapper vc_box_border_grey"><img width="600" height="342" src="/assets/images/trip/corporate-trip.jpg" class="vc_single_image-img attachment-full" alt="" srcset="
                              /assets/images/trip/corporate-trip.jpg" itemprop="image" /></a>
            </figure>
        </div>
        <div class="vc_custom_1553679771785">
            <div id="text-block-22" class="mk-text-block">
                <div class="mkdf-section-subtitle-holder mkdf-section-subtitle-center">
                    <p class="mkdf-section-subtitle">
                        Travelling is scientifically proven to make people
                        more productive. So it’s High time that you gather
                        your work colleagues&nbsp;and hit the divinely
                        soothing destinations to rejuvenate yourselves with
                        the best of nature with WanderOn’s customized group
                        trips.
                    </p>
                </div>
                <div class="clearboth"></div>
            </div>
        </div>
    </div>
</div>