<?php 

use Illuminate\Support\Facades\Route;

Route::get('/trip_gallery','Admin\TripGalleryController@index');
    Route::get('trip_gallery/add','Admin\TripGalleryController@add');
    Route::post('trip_gallery/add','Admin\TripGalleryController@store');
    Route::get('trip_gallery/edit/{id}','Admin\TripGalleryController@edit');
    Route::post('trip_gallery/edit/{id}','Admin\TripGalleryController@update');
    Route::get('trip_gallery/delete/{id}','Admin\TripGalleryController@delete');
?>