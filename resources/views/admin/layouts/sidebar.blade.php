<nav>
            <div class="main-navbar">
              <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
              <div id="mainnav">
                <ul class="nav-menu custom-scrollbar">
                  <li class="back-btn">
                    <div class="mobile-back text-right"><span>Back</span><i class="fa fa-angle-right pl-2" aria-hidden="true"></i></div>
                  </li>
                  <li class="dropdown"><a class="nav-link menu-title link-nav" href="/admin/dashboard"><i data-feather="anchor"></i><span>Dashboard</span></a></li>

                  <li class='dropdown'><a class='nav-link menu-title' href='#'><i data-feather='folder-plus'></i><span>Trip Type</span></a>
    <ul class='nav-submenu menu-content'>
        <li><a href='/admin/trip_type/add'>Create Trip Type</a></li>
        <li><a href='/admin/trip_type'>Trip Type</a></li>
    </ul>
</li>
<li class='dropdown'><a class='nav-link menu-title' href='#'><i data-feather='folder-plus'></i><span>Trip Review</span></a>
    <ul class='nav-submenu menu-content'>
        <li><a href='/admin/trip_review/add'>Create Trip Review</a></li>
        <li><a href='/admin/trip_review'>Trip Review</a></li>
    </ul>
</li>
<li class='dropdown'><a class='nav-link menu-title' href='#'><i data-feather='folder-plus'></i><span>Trip Iterinary</span></a>
    <ul class='nav-submenu menu-content'>
        <li><a href='/admin/trip_iterinary/add'>Create Trip Iterinary</a></li>
        <li><a href='/admin/trip_iterinary'>Trip Iterinary</a></li>
    </ul>
</li>
<li class='dropdown'><a class='nav-link menu-title' href='#'><i data-feather='folder-plus'></i><span>Trip Gallery</span></a>
    <ul class='nav-submenu menu-content'>
        <li><a href='/admin/trip_gallery/add'>Create Trip Gallery</a></li>
        <li><a href='/admin/trip_gallery'>Trip Gallery</a></li>
    </ul>
</li>
<li class='dropdown'><a class='nav-link menu-title' href='#'><i data-feather='folder-plus'></i><span>Trip Enquiry</span></a>
    <ul class='nav-submenu menu-content'>
        <li><a href='/admin/trip_enquiry/add'>Create Trip Enquiry</a></li>
        <li><a href='/admin/trip_enquiry'>Trip Enquiry</a></li>
    </ul>
</li>
<li class='dropdown'><a class='nav-link menu-title' href='#'><i data-feather='folder-plus'></i><span>Trip Category</span></a>
    <ul class='nav-submenu menu-content'>
        <li><a href='/admin/trip_category/add'>Create Trip Category</a></li>
        <li><a href='/admin/trip_category'>Trip Category</a></li>
    </ul>
</li>
<li class='dropdown'><a class='nav-link menu-title' href='#'><i data-feather='folder-plus'></i><span>Trip</span></a>
    <ul class='nav-submenu menu-content'>
        <li><a href='/admin/trip/add'>Create Trip</a></li>
        <li><a href='/admin/trip'>Trip</a></li>
    </ul>
</li>
<li class='dropdown'><a class='nav-link menu-title' href='#'><i data-feather='folder-plus'></i><span>Blog</span></a>
    <ul class='nav-submenu menu-content'>
        <li><a href='/admin/blog/add'>Create Blog</a></li>
        <li><a href='/admin/blog'>Blog</a></li>
    </ul>
</li>

                </ul>
              </div>
              <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
            </div>
          </nav>
