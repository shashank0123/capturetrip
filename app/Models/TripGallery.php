<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class TripGallery extends Model
{
protected $fillable = ["trip_id","image","status"];
}