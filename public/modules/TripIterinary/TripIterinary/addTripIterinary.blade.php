@extends('admin.layouts.admin')
@section('content')
<div class='col-md-12' style=''>
<Form action='/admin/trip_iterinary/add' method='post' enctype='multipart/form-data'>
@csrf
<div class='col-md-12'>
        <div class='form-group ui-draggable-handle' style='position: static;'>
            <label for='formcontrol-select1'>Trip</label>
            <select class='form-control btn-square' name='trip_id' id='trip_id'>
                @foreach ($trips as $item)
                    <option value='{{$item->id}}'>{{$item->trip_name}}</option>
                @endforeach

            </select>
            <p style='display: none' class='help-block'>Error Message</p>
        </div>
    </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Day</label>
                <input class='form-control btn-square' type='text' name='day' placeholder='Enter Day'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Details</label>
                <input class='form-control btn-square' type='text' name='details' placeholder='Enter Details'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Description</label>
                <input class='form-control btn-square' type='text' name='description' placeholder='Enter Description'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Status</label>
                <input class='form-control btn-square' type='text' name='status' placeholder='Enter Status'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
</div>
    <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <button class='btn btn-primary' type='submit' data-original-title='Save And Return' title=''>Save</button>
            <a href='/admin/trip_iterinary' class='btn btn-secondary' data-original-title='Cancel And Go Back' title=''>Cancel</a>
            <p style='display:none' class='help-block'>Press Submit to save</p>
        </div>
    </div>
    </Form>
@endsection