<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTripTable extends Migration

{
    public function up()
    {
        Schema::create('trips', function (Blueprint $table) {
            $table->id();
$table->string('trip_name')->nullable();
$table->string('description')->nullable();
$table->string('category_id')->nullable();
$table->string('cover_image')->nullable();
$table->string('cost')->nullable();
$table->string('cost_excludes')->nullable();
$table->string('cost_includes')->nullable();
$table->string('other_details')->nullable();
$table->string('start_price')->nullable();
$table->string(' trip_duration')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('trips');
    }
}
