<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\TripReview;
use App\Models\Trip;
use App\Http\Controllers\Controller;

class TripReviewController extends Controller
{
    public function index()
    {
        $data = TripReview::paginate(20);
        return view('admin.TripReview.listTripReview', compact('data'));
    }

    public function add()
    {
        
$trips = Trip::all();

        return view('admin.TripReview.addTripReview', compact('trips'));
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['trip_id'] = $data['trip_id'];
$saveData['name'] = $data['name'];
$saveData['email'] = $data['email'];
$saveData['phone'] = $data['phone'];
$saveData['title'] = $data['title'];
$saveData['rating'] = $data['rating'];
$saveData['details'] = $data['details'];


        $TripReview = TripReview::create($saveData);

        // return response()->json(['success' => true, 'data' => $TripReview], 200);
        return redirect('/admin/trip_review')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = TripReview::where('id', $id)->first();$trips = Trip::all();
return view('admin.TripReview.addTripReview', compact('row', 'trips'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['trip_id'] = $data['trip_id'];
$saveData['name'] = $data['name'];
$saveData['email'] = $data['email'];
$saveData['phone'] = $data['phone'];
$saveData['title'] = $data['title'];
$saveData['rating'] = $data['rating'];
$saveData['details'] = $data['details'];

        $row = TripReview::where('id', $id)->first();
        if ($row){
            $TripReview = TripReview::where('id', $id)->update($saveData);
        }
        return redirect('/admin/trip_review')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = TripReview::where('id', $request->id)->delete();
        return redirect('/admin/trip_review');

    }


    public function getData(){
        $data = TripReview::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
