<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Blog;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    public function index()
    {
        $data = Blog::paginate(20);
        return view('admin.Blog.listBlog', compact('data'));
    }

    public function add()
    {


        return view('admin.Blog.addBlog');
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
        $saveData['title'] = $data['title'];
        $saveData['text'] = $data['text'];

        if (request()->hasFile('featured_image')) {
            $path = request()->file('featured_image')->store(
                'file',
                'public'
            );

            $data['featured_image'] = \Storage::disk('public')->url($path);
        }
        $saveData['featured_image'] = $data['featured_image'];
        $saveData['status'] = $data['status'];


        $Blog = Blog::create($saveData);

        // return response()->json(['success' => true, 'data' => $Blog], 200);
        return redirect('/admin/blog')->with('successMsg', 'Data has been saved.');
    }

    public function edit($id)
    {
        $row = Blog::where('id', $id)->first();
        return view('admin.Blog.addBlog', compact('row',));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
        $saveData['title'] = $data['title'];
        $saveData['text'] = $data['text'];
        $saveData['featured_image'] = $data['featured_image'];
        $saveData['status'] = $data['status'];

        $row = Blog::where('id', $id)->first();
        if ($row) {
            $Blog = Blog::where('id', $id)->update($saveData);
        }
        return redirect('/admin/blog')->with('successMsg', 'Data has been updated.');
    }

    public function delete(Request $request)
    {
        $delete = Blog::where('id', $request->id)->delete();
        return redirect('/admin/blog');
    }


    public function getData()
    {
        $data = Blog::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
