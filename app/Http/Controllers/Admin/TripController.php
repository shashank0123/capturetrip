<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Trip;
use App\Models\TripCategory;
use App\Http\Controllers\Controller;

class TripController extends Controller
{
    public function index()
    {
        $data = Trip::paginate(20);
        return view('admin.Trip.listTrip', compact('data'));
    }

    public function add()
    {
        
$tripcategorys = TripCategory::all();

        return view('admin.Trip.addTrip', compact('tripcategorys'));
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['trip_name'] = $data['trip_name'];
$saveData['description'] = $data['description'];
$saveData['category_id'] = $data['category_id'];

 if (request()->hasFile('cover_image')) {
               $path = request()->file('cover_image')->store(
                   'file', 'public'
               );

               $data['cover_image'] = \Storage::disk('public')->url($path);

            }$saveData['cover_image'] = $data['cover_image'];
$saveData['cost'] = $data['cost'];
$saveData['cost_excludes'] = $data['cost_excludes'];
$saveData['cost_includes'] = $data['cost_includes'];
$saveData['other_details'] = $data['other_details'];
$saveData['start_price'] = $data['start_price'];
$saveData[' trip_duration'] = $data[' trip_duration'];


        $Trip = Trip::create($saveData);

        // return response()->json(['success' => true, 'data' => $Trip], 200);
        return redirect('/admin/trip')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = Trip::where('id', $id)->first();$tripcategorys = TripCategory::all();
return view('admin.Trip.addTrip', compact('row', 'tripcategorys'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['trip_name'] = $data['trip_name'];
$saveData['description'] = $data['description'];
$saveData['category_id'] = $data['category_id'];
$saveData['cover_image'] = $data['cover_image'];
$saveData['cost'] = $data['cost'];
$saveData['cost_excludes'] = $data['cost_excludes'];
$saveData['cost_includes'] = $data['cost_includes'];
$saveData['other_details'] = $data['other_details'];
$saveData['start_price'] = $data['start_price'];
$saveData[' trip_duration'] = $data[' trip_duration'];

        $row = Trip::where('id', $id)->first();
        if ($row){
            $Trip = Trip::where('id', $id)->update($saveData);
        }
        return redirect('/admin/trip')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = Trip::where('id', $request->id)->delete();
        return redirect('/admin/trip');

    }


    public function getData(){
        $data = Trip::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
