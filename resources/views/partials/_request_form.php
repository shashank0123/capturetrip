<div class=" vc_custom_1550756052894">
    <div id="text-block-34" class="mk-text-block   ">
        <h2>Request a Call Back</h2>
        <div class="clearboth"></div>
    </div>
</div>
<div class="wpb_raw_code wpb_content_element wpb_raw_html">
    <div class="wpb_wrapper">
        <div class="trip_page_form">
            <input type="text" name="name" id="trip_page_name" required="" autocomplete="on" class="trip_page_form_input" placeholder="Full Name">
            <input type="text" name="phone" id="trip_page_phone" required="" autocomplete="on" class="trip_page_form_input" placeholder="Your Phone Number">
            <input type="email" name="email" id="trip_page_email" required="" autocomplete="on" class="trip_page_form_input" placeholder="Your Email Id">
            <input type="text" name="destination" id="trip_page_destination" required="" class="trip_page_form_input" placeholder="Where do you wanna go?">
            <div class="trip_page_form_button">Send</div>
        </div>
    </div>
</div>