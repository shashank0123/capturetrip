<?php 

use Illuminate\Support\Facades\Route;

Route::get('/trip','Admin\TripController@index');
    Route::get('trip/add','Admin\TripController@add');
    Route::post('trip/add','Admin\TripController@store');
    Route::get('trip/edit/{id}','Admin\TripController@edit');
    Route::post('trip/edit/{id}','Admin\TripController@update');
    Route::get('trip/delete/{id}','Admin\TripController@delete');
?>