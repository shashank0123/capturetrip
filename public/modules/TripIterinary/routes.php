<?php 

use Illuminate\Support\Facades\Route;

Route::get('/trip_iterinary','Admin\TripIterinaryController@index');
    Route::get('trip_iterinary/add','Admin\TripIterinaryController@add');
    Route::post('trip_iterinary/add','Admin\TripIterinaryController@store');
    Route::get('trip_iterinary/edit/{id}','Admin\TripIterinaryController@edit');
    Route::post('trip_iterinary/edit/{id}','Admin\TripIterinaryController@update');
    Route::get('trip_iterinary/delete/{id}','Admin\TripIterinaryController@delete');
?>