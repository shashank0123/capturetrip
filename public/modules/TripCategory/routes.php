<?php 

use Illuminate\Support\Facades\Route;

Route::get('/trip_category','Admin\TripCategoryController@index');
    Route::get('trip_category/add','Admin\TripCategoryController@add');
    Route::post('trip_category/add','Admin\TripCategoryController@store');
    Route::get('trip_category/edit/{id}','Admin\TripCategoryController@edit');
    Route::post('trip_category/edit/{id}','Admin\TripCategoryController@update');
    Route::get('trip_category/delete/{id}','Admin\TripCategoryController@delete');
?>