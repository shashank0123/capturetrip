@extends('layouts.app')
@section('contents')
<div id="theme-page" class="master-holder  clearfix" itemscope="&quot;itemscope&quot;" itemtype="&quot;https://schema.org/Blog&quot;"><div class="master-holder-bg-holder"><div id="theme-page-bg" class="master-holder-bg js-el">

</div>
</div>
<div class="mk-main-wrapper-holder"><div id="mk-page-id-8075" class="theme-page-wrapper mk-main-wrapper mk-grid full-layout no-padding"><div class="theme-content no-padding" itemprop="mainEntityOfPage"><div data-mk-full-width="true" data-mk-full-width-init="true" data-mk-stretch-content="true" class="wpb_row vc_row vc_row-fluid  mk-fullwidth-true  attched-false     js-master-row  mk-full-content-true mk-in-viewport" style="position: relative; box-sizing: border-box; width: 1583px; left: -241.5px;"><div class="vc_col-sm-12 wpb_column column_container   _ "><div class="wpb_raw_code wpb_content_element wpb_raw_html"><div class="wpb_wrapper"><div class="text-wrapper"> WanderOn<div class="animated-words"> <span class="animated-words-span animated-words-span-5">Creating Stories</span></div>
</div>
</div>
</div>
<div class="wpb_raw_code wpb_raw_js"><div class="wpb_wrapper"> <script type="text/javascript">function changeBackpackingTripsHeading () {
var textArray = ['Connecting People','Creating Memories','Creating Stories','Fulfilling Adventure','Spreading Happiness'];
var i;
i=1;
setInterval(function(){
if(i>=textArray.length)
{i=0;}
document.querySelector('.animated-words-span-5').innerHTML = textArray[i];
i++;
}, 3000);
}
  changeBackpackingTripsHeading () ;</script> </div>

  </div>

</div>

</div>

<div class="vc_row-full-width vc_clearfix">
    </div>
    <div data-mk-stretch-content="true" class="wpb_row vc_row vc_row-fluid  mk-fullwidth-false  attched-false     js-master-row  mk-grid mk-in-viewport"><div class="vc_col-sm-12 wpb_column column_container   _ "><div class=" vc_custom_1572369251430"><div id="text-block-4" class="mk-text-block   "><p style="text-align: center;">With a plethora of destinations to choose from, we leave it to you to decide where you want to spend your holiday. Be it the adventurous roads of Himalayas or the soothing landscapes of North East India, we’ve curated the best itineraries for you that are laced with cherry-picked activities, comfortable accommodations and easy conveyance. What’s more? You get well-experienced team captains who ensure that all your trips are 100 percent safe and&nbsp;hassle-free. Have a look at our upcoming trips and plan your next vacation with WanderOn!</p><div class="clearboth"></div>
</div>
</div>
    <div id="padding-5" class="mk-padding-divider   clearfix"></div>
</div>
</div>
    <div data-mk-full-width="true" data-mk-full-width-init="true" data-mk-stretch-content="true" class="wpb_row vc_row vc_row-fluid  mk-fullwidth-true  attched-false    vc_custom_1569682542559  js-master-row  mk-full-content-true mk-in-viewport" style="position: relative; box-sizing: border-box; width: 1583px; left: -241.5px;"><div class="vc_col-sm-12 wpb_column column_container   _ "><div class="wpb_raw_code wpb_content_element wpb_raw_html"><div class="wpb_wrapper"><div class="trips_cards"> <a href="spititrip/" style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image trips_cards_items_image_spiti"><div style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_heading"><span style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_heading_span">Spiti Valley Trips</span></div>
        <div style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_costing"><img src="wp-content/uploads/2019/04/rupee.svg" style="height: 0.9em;">
            <br>Starting @ INR 18,999/-
        </div>
        <div style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_duration"><img src="wp-content/uploads/2019/04/calendar.svg" style="height: 0.9em;"><br>6 Nights/7 Days</div>
     </a> <a href="leh-ladakh-trips/" style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image trips_cards_items_image_ladakh"><div style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_heading"><span style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_heading_span">Leh Ladakh Trips</span></div>
        <div style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_costing"><img src="wp-content/uploads/2019/04/rupee.svg" style="height: 0.9em;"><br>Starting @ INR 31,499/-</div>
        <div style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_duration"><img src="wp-content/uploads/2019/04/calendar.svg" style="height: 0.9em;"><br>8 Nights/9 Days</div>
     </a> <a href="bhutan/" style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image trips_cards_items_image_bhutan"><div style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_heading"><span style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_heading_span">Bhutan RoadTrip</span></div>
        <div style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_costing"><img src="wp-content/uploads/2019/04/rupee.svg" style="height: 0.9em;"><br>Starting @ INR 23,999/-</div>
        <div style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_duration"><img src="wp-content/uploads/2019/04/calendar.svg" style="height: 0.9em;"><br>6 Nights/7 Days</div>
     </a> <a href="meghalayaroadtrip/" style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image trips_cards_items_image_meghalaya"><div style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_heading"><span style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_heading_span">Meghalaya RoadTrip</span></div>
        <div style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_costing"><img src="wp-content/uploads/2019/04/rupee.svg" style="height: 0.9em;"><br>Starting @ INR 17,999/-</div>
        <div style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_duration"><img src="wp-content/uploads/2019/04/calendar.svg" style="height: 0.9em;"><br>5 Nights/6 Days</div>
     </a> <a href="tawangroadtrip/" style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image trips_cards_items_image_tawang"><div style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_heading"><span style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_heading_span">Tawang RoadTrip</span></div>
        <div style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_costing"><img src="wp-content/uploads/2019/04/rupee.svg" style="height: 0.9em;"><br>Starting @ INR 25,999/-</div>
        <div style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_duration"><img src="wp-content/uploads/2019/04/calendar.svg" style="height: 0.9em;"><br>7 Nights/8 Days</div>
     </a> <a href="bali/" style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image trips_cards_items_image_bali"><div style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_heading"><span style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_heading_span">Bali Backpacking</span></div>
        <div style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_costing"><img src="wp-content/uploads/2019/04/rupee.svg" style="height: 0.9em;"><br>Starting @ INR 36,499/-</div>
        <div style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_duration"><img src="wp-content/uploads/2019/04/calendar.svg" style="height: 0.9em;"><br>5 Nights/6 Days</div>
     </a> <a href="chadartrek/" style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image trips_cards_items_image_chadar"><div style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_heading"><span style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_heading_span">Chadar Trek</span></div>
        <div style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_costing"><img src="wp-content/uploads/2019/04/rupee.svg" style="height: 0.9em;"><br>Starting @ INR 19,499/-</div>
        <div style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_duration"><img src="wp-content/uploads/2019/04/calendar.svg" style="height: 0.9em;"><br>8 Nights/9 Days</div>
     </a> <a href="sikkim-roadtrip/" style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image trips_cards_items_image_sikkim"><div style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_heading"><span style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_heading_span">Sikkim RoadTrip</span></div>
        <div style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_costing"><img src="wp-content/uploads/2019/04/rupee.svg" style="height: 0.9em;"><br>Starting @ INR 23,999/-</div>
        <div style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_duration"><img src="wp-content/uploads/2019/04/calendar.svg" style="height: 0.9em;"><br>6 Nights/7 Days</div>
     </a> <a href="himachalbackpacking/" style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image trips_cards_items_image_himachal"><div style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_heading"><span style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_heading_span">Himachal Backpacking</span></div>
        <div style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_costing"><img src="wp-content/uploads/2019/04/rupee.svg" style="height: 0.9em;"><br>Starting @ INR 22,499/-</div>
        <div style="background-image: linear-gradient(to top,rgba(0,0,0,.7) 15%,rgba(0,0,0,0) 30%),url(https://www.wanderon.in/wp-content/uploads/2019/02/wanderon-spiti-valley-monastery.jpg)" class="trips_cards_items_image_duration"><img src="wp-content/uploads/2019/04/calendar.svg" style="height: 0.9em;"><br>8 Nights/9 Days</div>
     </a></div>
</div>
</div>
</div>
</div>
    <div class="vc_row-full-width vc_clearfix"></div>
    <div class="clearboth"></div>
    <div class="clearboth"></div>
</div>
    <div class="clearboth"></div>
</div>
</div>
    <div class="racb_button">Request a Call Back</div>
</div>

@endsection
