<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class TripType extends Model
{
protected $fillable = ["trip_type","slug","status"];
}