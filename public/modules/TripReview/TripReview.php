<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class TripReview extends Model
{
protected $fillable = ["trip_id","name","email","phone","title","rating","details"];
}