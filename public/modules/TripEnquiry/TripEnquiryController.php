<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\TripEnquiry;
use App\Http\Controllers\Controller;

class TripEnquiryController extends Controller
{
    public function index()
    {
        $data = TripEnquiry::paginate(20);
        return view('admin.TripEnquiry.listTripEnquiry', compact('data'));
    }

    public function add()
    {
        

        return view('admin.TripEnquiry.addTripEnquiry', compact());
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['name'] = $data['name'];
$saveData['email'] = $data['email'];
$saveData['phone'] = $data['phone'];
$saveData['destination'] = $data['destination'];


        $TripEnquiry = TripEnquiry::create($saveData);

        // return response()->json(['success' => true, 'data' => $TripEnquiry], 200);
        return redirect('/admin/trip_enquiry')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = TripEnquiry::where('id', $id)->first();return view('admin.TripEnquiry.addTripEnquiry', compact('row', ));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['name'] = $data['name'];
$saveData['email'] = $data['email'];
$saveData['phone'] = $data['phone'];
$saveData['destination'] = $data['destination'];

        $row = TripEnquiry::where('id', $id)->first();
        if ($row){
            $TripEnquiry = TripEnquiry::where('id', $id)->update($saveData);
        }
        return redirect('/admin/trip_enquiry')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = TripEnquiry::where('id', $request->id)->delete();
        return redirect('/admin/trip_enquiry');

    }


    public function getData(){
        $data = TripEnquiry::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
