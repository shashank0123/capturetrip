<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTripGalleryTable extends Migration

{
    public function up()
    {
        Schema::create('trip_gallerys', function (Blueprint $table) {
            $table->id();
$table->string('trip_id')->nullable();
$table->string('image')->nullable();
$table->string('status')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('trip_gallerys');
    }
}
