@extends('layouts.app')
@section('contents')
 <div id="theme-page" class="master-holder clearfix" itemscope='"itemscope"' itemtype='"https://schema.org/Blog"'>
        <div class="master-holder-bg-holder">
          <div id="theme-page-bg" class="master-holder-bg js-el" data-mk-component="Parallax" data-parallax-config='{"speed" : 0.3 }' style="
                background-attachment: scroll;
                will-change: transform;
                transform: translateY(-12.3px) translateZ(0px);
                height: 2238.65px;
              "></div>
        </div>
        <div class="mk-main-wrapper-holder">
          <div id="mk-page-id-82" class="theme-page-wrapper mk-main-wrapper full-width-layout no-padding">
            <div class="theme-content no-padding" itemprop="mainEntityOfPage">
              <div class="wpb_row vc_row vc_row-fluid mk-fullwidth-true attched-false js-master-row mk-full-content-true mk-in-viewport">
                <div class="vc_col-sm-12 wpb_column column_container ">
                  <div class="vc_custom_1582378813304">
                    <div id="text-block-3" class="mk-text-block">
                      <div id="metaslider-id-5414" style="width: 100%; margin: 0 auto;" class="ml-slider-3-15-3 metaslider metaslider-nivo metaslider-5414 ml-slider">
                        <div id="metaslider_container_5414">
                          <div class="slider-wrapper theme-default">
                            <div class="ribbon"></div>
                            <div id="metaslider_5414" class="nivoSlider">
                                <a href="leh-ladakh-trips/" target="_blank" class="nivo-imageLink" style="display: none;">
                                    <img src="assets/images/slider/slider-1.png" height="763" width="1920" title="wanderon-leh-ladakh-2020" alt="wanderon-leh-ladakh-2020" class="slider-5414 slide-40198" style="width: 1583px;visibility: hidden;display: inline;" />
                                </a>
                                <a href="spititrip/" target="_blank" class="nivo-imageLink" style="display: none;">
                                    <img src="assets/images/slider/slider-1.png" height="763" width="1920" title="wanderon-spiti-valley-2020" alt="wanderon-spiti-valley-2020" class="slider-5414 slide-40200" style="width: 1583px;visibility: hidden;display: inline;" />
                                </a>
                                <a href="bali/" target="_blank" class="nivo-imageLink" style="display: none;">
                                <img src="assets/images/slider/slider-1.png" height="763" width="1920" title="wanderon-bali-2020" alt="wanderon-bali-2020" class="slider-5414 slide-40195" style="
                                      width: 1583px;
                                      visibility: hidden;
                                      display: inline;
                                    " /></a>

                                    <img class="nivo-main-image" src="assets/images/slider/slider-1.png" alt="wanderon-meghalaya-roadtrip-2020" style="
                                    display: inline;
                                    height: 629px;
                                    overflow: hidden;
                                  " />
                              <div class="nivo-caption"></div>
                              <div class="nivo-directionNav">
                                <a class="nivo-prevNav">Previous</a><a class="nivo-nextNav">Next</a>
                              </div>
                              <div class="nivo-slice" name="0" style="
                                    width: 1104.14px;
                                    height: 629px;
                                    opacity: 1;
                                    overflow: hidden;
                                    right: 0px;
                                  ">
                                <img src="assets/images/slider/slider-1.png" style="
                                      position: absolute;
                                      width: 1583px;
                                      height: auto;
                                      display: block !important;
                                      top: 0;
                                      left: -0px;
                                    " />
                              </div>
                              <div class="nivo-slice" name="1" style="
                                    left: 106px;
                                    width: 106px;
                                    height: 629px;
                                    opacity: 0;
                                    overflow: hidden;
                                  ">
                                <img src="assets/images/slider/slider-1.png" style="
                                      position: absolute;
                                      width: 1583px;
                                      height: auto;
                                      display: block !important;
                                      top: 0;
                                      left: -106px;
                                    " />
                              </div>
                              <div class="nivo-slice" name="2" style="
                                    left: 212px;
                                    width: 106px;
                                    height: 629px;
                                    opacity: 0;
                                    overflow: hidden;
                                  ">
                                <img src="assets/images/slider/slider-1.png" style="
                                      position: absolute;
                                      width: 1583px;
                                      height: auto;
                                      display: block !important;
                                      top: 0;
                                      left: -212px;
                                    " />
                              </div>
                              <div class="nivo-slice" name="3" style="
                                    left: 318px;
                                    width: 106px;
                                    height: 629px;
                                    opacity: 0;
                                    overflow: hidden;
                                  ">
                                <img src="assets/images/slider/slider-1.png" style="
                                      position: absolute;
                                      width: 1583px;
                                      height: auto;
                                      display: block !important;
                                      top: 0;
                                      left: -318px;
                                    " />
                              </div>
                              <div class="nivo-slice" name="4" style="
                                    left: 424px;
                                    width: 106px;
                                    height: 629px;
                                    opacity: 0;
                                    overflow: hidden;
                                  ">
                                <img src="assets/images/slider/slider-1.png" style="
                                      position: absolute;
                                      width: 1583px;
                                      height: auto;
                                      display: block !important;
                                      top: 0;
                                      left: -424px;
                                    " />
                              </div>
                              <div class="nivo-slice" name="5" style="
                                    left: 530px;
                                    width: 106px;
                                    height: 629px;
                                    opacity: 0;
                                    overflow: hidden;
                                  ">
                                <img src="assets/images/slider/slider-1.png" style="
                                      position: absolute;
                                      width: 1583px;
                                      height: auto;
                                      display: block !important;
                                      top: 0;
                                      left: -530px;
                                    " />
                              </div>
                              <div class="nivo-slice" name="6" style="
                                    left: 636px;
                                    width: 106px;
                                    height: 629px;
                                    opacity: 0;
                                    overflow: hidden;
                                  ">
                                <img src="assets/images/slider/slider-1.png" style="
                                      position: absolute;
                                      width: 1583px;
                                      height: auto;
                                      display: block !important;
                                      top: 0;
                                      left: -636px;
                                    " />
                              </div>
                              <div class="nivo-slice" name="7" style="
                                    left: 742px;
                                    width: 106px;
                                    height: 629px;
                                    opacity: 0;
                                    overflow: hidden;
                                  ">
                                <img src="assets/images/slider/slider-1.png" style="
                                      position: absolute;
                                      width: 1583px;
                                      height: auto;
                                      display: block !important;
                                      top: 0;
                                      left: -742px;
                                    " />
                              </div>
                              <div class="nivo-slice" name="8" style="
                                    left: 848px;
                                    width: 106px;
                                    height: 629px;
                                    opacity: 0;
                                    overflow: hidden;
                                  ">
                                <img src="assets/images/slider/slider-1.png" style="
                                      position: absolute;
                                      width: 1583px;
                                      height: auto;
                                      display: block !important;
                                      top: 0;
                                      left: -848px;
                                    " />
                              </div>
                              <div class="nivo-slice" name="9" style="
                                    left: 954px;
                                    width: 106px;
                                    height: 629px;
                                    opacity: 0;
                                    overflow: hidden;
                                  ">
                                <img src="assets/images/slider/slider-1.png" style="
                                      position: absolute;
                                      width: 1583px;
                                      height: auto;
                                      display: block !important;
                                      top: 0;
                                      left: -954px;
                                    " />
                              </div>
                              <div class="nivo-slice" name="10" style="
                                    left: 1060px;
                                    width: 106px;
                                    height: 629px;
                                    opacity: 0;
                                    overflow: hidden;
                                  ">
                                <img src="assets/images/slider/slider-1.png" style="
                                      position: absolute;
                                      width: 1583px;
                                      height: auto;
                                      display: block !important;
                                      top: 0;
                                      left: -1060px;
                                    " />
                              </div>
                              <div class="nivo-slice" name="11" style="
                                    left: 1166px;
                                    width: 106px;
                                    height: 629px;
                                    opacity: 0;
                                    overflow: hidden;
                                  ">
                                <img src="assets/images/slider/slider-1.png" style="
                                      position: absolute;
                                      width: 1583px;
                                      height: auto;
                                      display: block !important;
                                      top: 0;
                                      left: -1166px;
                                    " />
                              </div>
                              <div class="nivo-slice" name="12" style="
                                    left: 1272px;
                                    width: 106px;
                                    height: 629px;
                                    opacity: 0;
                                    overflow: hidden;
                                  ">
                                <img src="assets/images/slider/slider-1.png" style="
                                      position: absolute;
                                      width: 1583px;
                                      height: auto;
                                      display: block !important;
                                      top: 0;
                                      left: -1272px;
                                    " />
                              </div>
                              <div class="nivo-slice" name="13" style="
                                    left: 1378px;
                                    width: 106px;
                                    height: 629px;
                                    opacity: 0;
                                    overflow: hidden;
                                  ">
                                <img src="assets/images/slider/slider-1.png" style="
                                      position: absolute;
                                      width: 1583px;
                                      height: auto;
                                      display: block !important;
                                      top: 0;
                                      left: -1378px;
                                    " />
                              </div>
                              <div class="nivo-slice" name="14" style="
                                    left: 1484px;
                                    width: 99px;
                                    height: 629px;
                                    opacity: 0;
                                    overflow: hidden;
                                  ">
                                <img src="assets/images/slider/slider-1.png" style="
                                      position: absolute;
                                      width: 1583px;
                                      height: auto;
                                      display: block !important;
                                      top: 0;
                                      left: -1484px;
                                    " />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="clearboth"></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="wpb_row vc_row vc_row-fluid mk-fullwidth-false attched-false js-master-row mk-grid">
                <div class="vc_col-sm-12 wpb_column column_container">
                  <div class="wpb_raw_code wpb_content_element wpb_raw_html">
                    <div class="wpb_wrapper">
                      <div id="upcoming_community_trips" class="all_web_headings">
                        Upcoming Community Group Trips
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="wpb_row vc_row vc_row-fluid mk-fullwidth-true attched-false vc_custom_1569595094188 js-master-row mk-full-content-true">
                <div class="vc_col-sm-12 wpb_column column_container ">
                  <div class="wpb_raw_code wpb_content_element wpb_raw_html">
                    <div class="wpb_wrapper">
                      <div class="homepage_our_trips">
                        <a href="backpacking-trips/" class="homepage_our_trips_items">
                          <div class="homepage_our_trips_items_image homepage_our_trips_items_image_backpacking">
                            <div class="homepage_our_trips_items_image_heading">
                              <span class="homepage_our_trips_items_image_heading_span">Backpacking Trips</span>
                            </div>
                            <div class="homepage_our_trips_items_image_costing">
                              <img src="wp-content/uploads/2019/04/rupee.svg" style="height: 0.9em;" /><br />Starting @ INR 17,999/-
                            </div>
                            <div class="homepage_our_trips_items_image_duration">
                              <img src="wp-content/uploads/2019/04/calendar.svg" style="height: 0.9em;" /><br />6+ Days
                            </div>
                          </div>
                          <!--<div class="homepage_our_trips_items_description">Weeklong getaways from the monotony of Life and escape to serene destinations</div>-->
                        </a>
                        <a href="weekendgetaways/" class="homepage_our_trips_items">
                          <div class="homepage_our_trips_items_image homepage_our_trips_items_image_delhi">
                            <div class="homepage_our_trips_items_image_heading">
                              <span class="homepage_our_trips_items_image_heading_span">Weekend Getaways from Delhi</span>
                            </div>
                            <div class="homepage_our_trips_items_image_costing">
                              <img src="wp-content/uploads/2019/04/rupee.svg" style="height: 0.9em;" /><br />Starting @ INR 5,499/-
                            </div>
                            <div class="homepage_our_trips_items_image_duration">
                              <img src="wp-content/uploads/2019/04/calendar.svg" style="height: 0.9em;" /><br />2+ Days
                            </div>
                          </div>
                          <!--<div class="homepage_our_trips_items_description">Short weekend break from the capital city to unburden worries behind</div>-->
                        </a>
                        <a href="weekend-getaways-from-bangalore/" class="homepage_our_trips_items">
                          <div class="homepage_our_trips_items_image homepage_our_trips_items_image_bangalore_lazy">
                            <div class="homepage_our_trips_items_image_heading">
                              <span class="homepage_our_trips_items_image_heading_span">Weekend Getaways from Bangalore</span>
                            </div>
                            <div class="homepage_our_trips_items_image_costing">
                              <img src="wp-content/uploads/2019/04/rupee.svg" style="height: 0.9em;" /><br />Starting @ INR 3,999/-
                            </div>
                            <div class="homepage_our_trips_items_image_duration">
                              <img src="wp-content/uploads/2019/04/calendar.svg" style="height: 0.9em;" /><br />2+ Days
                            </div>
                          </div>
                          <!--<div class="homepage_our_trips_items_description">Take a break from daunting IT lives and experience the magnificence of southern India</div>-->
                        </a>
                        <a href="himalayan-escapades/" class="homepage_our_trips_items">
                          <div class="homepage_our_trips_items_image homepage_our_trips_items_image_escapades_lazy">
                            <div class="homepage_our_trips_items_image_heading">
                              <span class="homepage_our_trips_items_image_heading_span">Himalayan Escapades</span>
                            </div>
                            <div class="homepage_our_trips_items_image_costing">
                              <img src="wp-content/uploads/2019/04/rupee.svg" style="height: 0.9em;" /><br />Starting @ INR 11,499/-
                            </div>
                            <div class="homepage_our_trips_items_image_duration">
                              <img src="wp-content/uploads/2019/04/calendar.svg" style="height: 0.9em;" /><br />4+ Days
                            </div>
                          </div>
                          <!--<div class="homepage_our_trips_items_description">Lose yourself amidst the beauty of aesthetic mountain ranges</div>-->
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="wpb_row vc_row vc_row-fluid mk-fullwidth-false attched-false js-master-row mk-grid">
                <div class="vc_col-sm-12 wpb_column column_container ">
                  <div class="wpb_raw_code wpb_content_element wpb_raw_html">
                    <div class="wpb_wrapper">
                      <div id="booking_id_generator" style="display: none;">
                        0
                      </div>
                    </div>
                  </div>
                  <div class="wpb_raw_code wpb_content_element wpb_raw_html">
                    <div class="wpb_wrapper">
                      <div class="all_web_headings" style="margin-bottom: -10px;">
                        Plan a Customized Trip
                      </div>
                    </div>
                  </div>
                  <div class="vc_custom_1570096376772">
                    <div id="text-block-7" class="mk-text-block">
                      <p style="text-align: center;">
                        Didn’t find what you were looking for? Have a look at
                        our customized trips curated for your group and plan
                        your next vacation with WanderOn!
                      </p>
                      <div class="clearboth"></div>
                    </div>
                  </div>
                  <div class="wpb_raw_code wpb_content_element wpb_raw_html">
                    <div class="wpb_wrapper">
                      <div class="homepage_byog">
                        <a href="andaman/" class="homepage_byog_items homepage_byog_items_andaman_lazy" style="text-decoration-line: none;" target="_blank">
                          <span class="homepage_byog_items_heading_span"><span class="homepage_byog_items_heading">Andaman</span></span>
                        </a>
                        <a href="himachaltrips/" class="homepage_byog_items homepage_byog_items_himachal_lazy" style="text-decoration-line: none;" target="_blank">
                          <span class="homepage_byog_items_heading_span"><span class="homepage_byog_items_heading">Himachal</span></span>
                        </a>
                        <a href="kashmirtrips/" class="homepage_byog_items homepage_byog_items_kashmir_lazy" style="text-decoration-line: none;" target="_blank">
                          <span class="homepage_byog_items_heading_span"><span class="homepage_byog_items_heading">Kashmir</span></span>
                        </a>
                        <a href="keralatrips/" class="homepage_byog_items homepage_byog_items_kerala_lazy" style="text-decoration-line: none;" target="_blank">
                          <span class="homepage_byog_items_heading_span"><span class="homepage_byog_items_heading">Kerala</span></span>
                        </a>
                        <a href="northeast/" class="homepage_byog_items homepage_byog_items_north_east_lazy" style="text-decoration-line: none;" target="_blank">
                          <span class="homepage_byog_items_heading_span"><span class="homepage_byog_items_heading">North East</span></span>
                        </a>
                        <a href="uttarakhandtrips/" class="homepage_byog_items homepage_byog_items_uttarakhand_lazy" style="text-decoration-line: none;" target="_blank">
                          <span class="homepage_byog_items_heading_span"><span class="homepage_byog_items_heading">Uttarakhand</span></span>
                        </a>
                      </div>
                    </div>
                  </div>
                  <div id="padding-8" class="mk-padding-divider clearfix"></div>
                </div>
              </div>
              <div class="wpb_row vc_row vc_row-fluid mk-fullwidth-true attched-false js-master-row mk-full-content-true">
                <div class="vc_col-sm-12 wpb_column column_container ">
                  <div class="wpb_raw_code wpb_content_element wpb_raw_html">
                    <div class="wpb_wrapper">
                      <div class="all_web_headings">
                        Build Your Bucket List
                      </div>
                      <div class="blog_list_container">
                        <a href="places-to-visit-on-long-dussehra-weekend/" class="blog_list_item">
                          <div class="blog_list_item_text">
                            <span class="blog_list_item_title">7 Places to Visit On This Long Dussehra
                              Weekend</span>
                            <span class="blog_list_item_description"><img src="wp-content/uploads/2019/04/clock.svg" style="height: 0.9em;" />&nbsp;5 minutes read</span>
                          </div>
                          <div class="blog_list_item_image blog_list_item_image_dussehra_lazy"></div>
                        </a>
                        <a href="exotic-beaches-near-bangalore-for-weekend-getaway/" class="blog_list_item">
                          <div class="blog_list_item_text">
                            <span class="blog_list_item_title">Weekend Getaway Destinations From Bangalore For
                              a Beach Holiday</span>
                            <span class="blog_list_item_description"><img src="wp-content/uploads/2019/04/clock.svg" style="height: 0.9em;" />&nbsp;5 minutes read</span>
                          </div>
                          <div class="blog_list_item_image blog_list_item_image_bangalore_lazy"></div>
                        </a>
                        <a href="life-lessons-to-take-from-bhutan-trip/" class="blog_list_item">
                          <div class="blog_list_item_text">
                            <span class="blog_list_item_title">Life Lessons To Take From Bhutan Trip</span>
                            <span class="blog_list_item_description"><img src="wp-content/uploads/2019/04/clock.svg" style="height: 0.9em;" />&nbsp;4 minutes read</span>
                          </div>
                          <div class="blog_list_item_image blog_list_item_image_bhutan_lazy"></div>
                        </a>
                        <a href="scenic-waterfalls-to-visit-in-meghalaya/" class="blog_list_item">
                          <div class="blog_list_item_text">
                            <span class="blog_list_item_title">10 Scenic Waterfalls of Meghalaya To Fall In
                              Love With</span>
                            <span class="blog_list_item_description"><img src="wp-content/uploads/2019/04/clock.svg" style="height: 0.9em;" />&nbsp;6 minutes read</span>
                          </div>
                          <div class="blog_list_item_image blog_list_item_image_meghalaya_lazy"></div>
                        </a>
                        <a href="why-you-should-do-chadar-trek-in-2020/" class="blog_list_item">
                          <div class="blog_list_item_text">
                            <span class="blog_list_item_title">Reasons Which Will Make You Fall With Chadar
                              Trek in 2020</span>
                            <span class="blog_list_item_description"><img src="wp-content/uploads/2019/04/clock.svg" style="height: 0.9em;" />&nbsp;4 minutes read</span>
                          </div>
                          <div class="blog_list_item_image blog_list_item_image_chadar_lazy"></div>
                        </a>
                        <a href="sikkim-worlds-first-organic-state/" class="blog_list_item">
                          <div class="blog_list_item_text">
                            <span class="blog_list_item_title">Another Reason to Visit Sikkim - World's First
                              Organic State</span>
                            <span class="blog_list_item_description"><img src="wp-content/uploads/2019/04/clock.svg" style="height: 0.9em;" />&nbsp;6 minutes read</span>
                          </div>
                          <div class="blog_list_item_image blog_list_item_image_sikkim_lazy"></div>
                        </a>
                      </div>
                      <a href="blogs/" target="_blank">
                        <div class="any_button_div">
                          <span class="any_button">Read more</span>
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="wpb_row vc_row vc_row-fluid mk-fullwidth-false attched-false js-master-row mk-grid">
                <div class="vc_col-sm-12 wpb_column column_container ">
                  <div class="wpb_raw_code wpb_raw_js">
                    <div class="wpb_wrapper">
                      <script type="text/javascript">
                        var back1 = document.querySelector(
                          ".homepage_our_trips_items_image_bangalore_lazy"
                        );
                        var back2 = document.querySelector(
                          ".homepage_our_trips_items_image_escapades_lazy"
                        );
                        var back3 = document.querySelector(
                          ".homepage_byog_items_andaman_lazy"
                        );
                        var back4 = document.querySelector(
                          ".homepage_byog_items_himachal_lazy"
                        );
                        var back5 = document.querySelector(
                          ".homepage_byog_items_kashmir_lazy"
                        );
                        var back6 = document.querySelector(
                          ".homepage_byog_items_kerala_lazy"
                        );
                        var back7 = document.querySelector(
                          ".homepage_byog_items_north_east_lazy"
                        );
                        var back8 = document.querySelector(
                          ".homepage_byog_items_uttarakhand_lazy"
                        );
                        var back9 = document.querySelector(
                          ".blog_list_item_image_dussehra_lazy"
                        );
                        var back10 = document.querySelector(
                          ".blog_list_item_image_bangalore_lazy"
                        );
                        var back11 = document.querySelector(
                          ".blog_list_item_image_bhutan_lazy"
                        );
                        var back12 = document.querySelector(
                          ".blog_list_item_image_meghalaya_lazy"
                        );
                        var back13 = document.querySelector(
                          ".blog_list_item_image_chadar_lazy"
                        );
                        var back14 = document.querySelector(
                          ".blog_list_item_image_sikkim_lazy"
                        );
                        window.onscroll = function() {
                          if (
                            document.body.scrollTop > 350 ||
                            document.documentElement.scrollTop > 350
                          ) {
                            back1.style.backgroundImage =
                              "linear-gradient(to top, rgba(0,0,0,0.7) 15%, rgba(0,0,0,0) 30%), url('wp-content/uploads/2019/09/wanderon-gokarna-sunset.jpg')";
                            back2.style.backgroundImage =
                              "linear-gradient(to top, rgba(0,0,0,0.7) 15%, rgba(0,0,0,0) 30%), url('wp-content/uploads/2019/07/himachal.jpg')";
                            back3.style.backgroundImage =
                              "url('wp-content/uploads/2019/03/andamanescapade.jpg')";
                            back4.style.backgroundImage =
                              "url('wp-content/uploads/2019/03/tirthan-valley-hidden-gem-of-himachal-1-13.jpg')";
                            back5.style.backgroundImage =
                              "url('wp-content/uploads/2019/03/kashmirdelight.jpg')";
                            back6.style.backgroundImage =
                              "url('wp-content/uploads/2019/03/gemsofkeralawithalleppey.jpg')";
                            back7.style.backgroundImage =
                              "url('wp-content/uploads/2019/03/meghalayawithkaziranga.jpg')";
                            back8.style.backgroundImage =
                              "url('wp-content/uploads/2019/03/magnificientuttarakhand.jpg')";
                            back9.style.backgroundImage =
                              "url('wp-content/uploads/2019/10/places-to-visit-on-this-long-dussehra-weekend.jpg')";
                            back10.style.backgroundImage =
                              "url('wp-content/uploads/2019/10/goa.jpg')";
                            back11.style.backgroundImage =
                              "url('wp-content/uploads/2019/10/learn-to-laugh-even-at-yourself.jpg')";
                            back12.style.backgroundImage =
                              "url('wp-content/uploads/2019/10/falls-in-meghalaya.jpg')";
                            back13.style.backgroundImage =
                              "url('wp-content/uploads/2019/10/witness-a-whole-new-world-in-chadar-trek-1.jpg')";
                            back14.style.backgroundImage =
                              "url('wp-content/uploads/2019/10/sikkim-travel-package.jpg')";
                          }
                        };
                      </script>
                    </div>
                  </div>
                </div>
              </div>
              <div class="wpb_row vc_row vc_row-fluid mk-fullwidth-false attched-false js-master-row mk-grid">
                <div class="vc_col-sm-12 wpb_column column_container ">
                  <div id="padding-12" class="mk-padding-divider clearfix"></div>
                  <div class="vc_custom_1573641186536">
                    <div id="text-block-13" class="mk-text-block">
                      <h1 class="mkdf-section-title mkdf-section-title-large" style="text-align: center; color: black;">
                        We take care of Everything
                      </h1>
                      <div class="mkdf-section-subtitle-holder mkdf-section-subtitle-center">
                        <p style="text-align: center;">
                          Travel along the picturesque destinations, indulge
                          in thrilling activities and enjoy the charm of
                          nature without worrying about the minutest of issues
                          regarding your travel plans! Our highly skilled and
                          experienced group captains arrange all that you
                          need, while your eyes and body take a treat from
                          nature. Bring together your friends, family or
                          colleagues and be ready to collect innumerable
                          moments that always have an element of serendipity
                          on our amazing group trips!
                        </p>
                      </div>
                      <div class="clearboth"></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="wpb_row vc_row vc_row-fluid mk-fullwidth-false attched-false js-master-row mk-grid">
    <div class="vc_col-sm-4 vc_col-lg-4 vc_col-md-4 wpb_column column_container _ height-full">
        <div class="vc_custom_1553679745464">
            <div id="text-block-15" class="mk-text-block">
                <h2 class="mkdf-section-title mkdf-section-title-large" style="text-align: center; color: black;">
                    Family Trip
                </h2>
                <div class="clearboth"></div>
            </div>
        </div>
        <div class="wpb_single_image wpb_content_element vc_align_center">
            <figure class="wpb_wrapper vc_figure">
                <a href="/enquiry-page.php" target="_self" class="vc_single_image-wrapper vc_box_border_grey"><img width="600" height="342" src="wp-content/uploads/2020/01/family-trip.jpg" class="vc_single_image-img attachment-full" alt="" srcset="
                              wp-content/uploads/2020/01/family-trip.jpg         600w,
                              wp-content/uploads/2020/01/family-trip-300x171.jpg 300w
                            " sizes="(max-width: 600px) 100vw, 600px" itemprop="image" /></a>
            </figure>
        </div>
        <div class="vc_custom_1553679737105">
            <div id="text-block-16" class="mk-text-block">
                <div class="mkdf-section-subtitle-holder mkdf-section-subtitle-center">
                    <p class="mkdf-section-subtitle">
                        Grandparents, parents, siblings, uncles, aunts-
                        everyone has a unique picture of their perfect trip
                        which surely makes planning a family trip one hell
                        of a nightmare. With our expertise, all you need is
                        to give us are your dates, and we’ll figure out the
                        best plan for your family!
                    </p>
                </div>
                <div class="clearboth"></div>
            </div>
        </div>
    </div>
    <div class="vc_col-sm-4 vc_col-lg-4 vc_col-md-4 wpb_column column_container _ height-full">
        <div class="vc_custom_1553679762427">
            <div id="text-block-18" class="mk-text-block">
                <h2 class="mkdf-section-title mkdf-section-title-large" style="text-align: center; color: black;">
                    College Trip
                </h2>
                <div class="clearboth"></div>
            </div>
        </div>
        <div class="wpb_single_image wpb_content_element vc_align_center">
            <figure class="wpb_wrapper vc_figure">
                <a href="/enquiry-page.php" target="_self" class="vc_single_image-wrapper vc_box_border_grey"><img width="600" height="342" src="wp-content/uploads/2020/01/college-trip-2.jpg" class="vc_single_image-img attachment-full" alt="" srcset="
                              wp-content/uploads/2020/01/college-trip-2.jpg         600w,
                              wp-content/uploads/2020/01/college-trip-2-300x171.jpg 300w
                            " sizes="(max-width: 600px) 100vw, 600px" itemprop="image" /></a>
            </figure>
        </div>
        <div class="vc_custom_1553679756270">
            <div id="text-block-19" class="mk-text-block">
                <div class="mkdf-section-subtitle-holder mkdf-section-subtitle-center">
                    <p class="mkdf-section-subtitle">
                        Dilemmas in deciding where to go, get the best
                        travel memories and keep your bank accounts, alive
                        at the same time? Keep your hassles aside and create
                        the finest travel memories with your college buddies
                        through our specially curated trips for college
                        students.
                    </p>
                </div>
                <div class="clearboth"></div>
            </div>
        </div>
    </div>
    <div class="vc_col-sm-4 vc_col-lg-4 vc_col-md-4 wpb_column column_container _ height-full">
        <div class="vc_custom_1553679778045">
            <div id="text-block-21" class="mk-text-block">
                <h2 class="mkdf-section-title mkdf-section-title-large" style="text-align: center; color: black;">
                    Corporate Trip
                </h2>
                <div class="clearboth"></div>
            </div>
        </div>
        <div class="wpb_single_image wpb_content_element vc_align_center">
            <figure class="wpb_wrapper vc_figure">
                <a href="/enquiry-page.php" target="_self" class="vc_single_image-wrapper vc_box_border_grey"><img width="600" height="342" src="/assets/images/trip/corporate-trip.jpg" class="vc_single_image-img attachment-full" alt="" srcset="
                              /assets/images/trip/corporate-trip.jpg" itemprop="image" /></a>
            </figure>
        </div>
        <div class="vc_custom_1553679771785">
            <div id="text-block-22" class="mk-text-block">
                <div class="mkdf-section-subtitle-holder mkdf-section-subtitle-center">
                    <p class="mkdf-section-subtitle">
                        Travelling is scientifically proven to make people
                        more productive. So it’s High time that you gather
                        your work colleagues&nbsp;and hit the divinely
                        soothing destinations to rejuvenate yourselves with
                        the best of nature with WanderOn’s customized group
                        trips.
                    </p>
                </div>
                <div class="clearboth"></div>
            </div>
        </div>
    </div>
</div>              <div class="clearboth"></div>
              <div class="clearboth"></div>
            </div>
            <div class="clearboth"></div>
          </div>
        </div>
        <div class="racb_button">Request a Call Back</div>
      </div>
      <section id="mk-footer-unfold-spacer"></section>
@endsection
